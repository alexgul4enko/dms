var UsersTT = require('../services/UsersTT');
var validator = require ('./coocieValidete');


module.exports = cp=>{
  return {
    get:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
      UsersTT.get({login,pass},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(502).send(err);
        })
    },
    post:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
      const {usr, tt} = req.body || {};
      if(!usr || !tt ){
        res.status(400).send('Пустой запрос'); 
      }
      UsersTT.post({login,pass,usr,tt},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(502).send(err);
        })
    },
    
    del:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
      const {id} = req.params;
      if(!parseInt(id)){
        res.status(400).send('Пустой запрос'); 
      }
      UsersTT.dell({login,pass,id},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    put:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
      const {id} = req.params;
      const {period} = req.body;
      if(!parseInt(id)){
        res.status(400).send('Пустой запрос'); 
      }
      UsersTT.put({login,pass,id,period},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    }
  }
}




