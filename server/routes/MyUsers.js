var MyUsers = require('../services/MyUsers');
var validator = require ('./coocieValidete');
var fs = require('fs');
var path = require("path");
const {files_folder} = require('../../properties.js');

module.exports = cp=>{
  return {
    get:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :login,pass,ID :id} = JSON.parse(req.signedCookies.UserDao);
      let data = {login,pass,id};
      MyUsers.get(data,cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    post:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :c_login,pass :c_pass,ID :id} = JSON.parse(req.signedCookies.UserDao);
      const {FirstName, LastName,login,mail,pass} = req.body || {};
      if(!FirstName || !LastName || !login || !mail || !pass){
        res.status(400).send('Пустой запрос'); 
      }
      MyUsers.post({c_login,c_pass,id ,FirstName, LastName,login,mail,pass ,host:req.headers.host},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    put:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :c_login,pass :c_pass} = JSON.parse(req.signedCookies.UserDao);
      const {FirstName, LastName,login,mail,pass} = req.body || {};
      const {id} = req.params;
      if(!FirstName || !LastName || !login || !mail || !pass || !parseInt(id)){
        res.status(400).send('Пустой запрос'); 
        return;
      }
      MyUsers.put({c_login,c_pass,id ,FirstName, LastName,login,mail,pass},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    del:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :c_login,pass :c_pass} = JSON.parse(req.signedCookies.UserDao);
      const {act} = req.body || {};
      const {id} = req.params;
      if(!parseInt(id)){
        res.status(400).send('Пустой запрос'); 
      }
      MyUsers.dell({c_login,c_pass,id, act},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    putAvatar:(req, res, next)=>{
      validator(req, res);
      const {UserLogin :c_login,pass :c_pass} = JSON.parse(req.signedCookies.UserDao);
      const {login,avatar,photo} = req.body || {};
      const {id} = req.params;
      if(!login || !parseInt(id)){
        res.status(400).send('Пустой запрос'); 
      }

      MyUsers.putAvatar({c_login,c_pass,id ,login,avatar,photo,id},cp)
        .then(data_=>{
          res.status(200).json(data_);
        })
        .catch(err=>{
            res.status(err==="empty"?502:501).send(err);
        })
    },
    getImage:(req, res, next)=>{
      validator(req, res);
      const filePath = path.join(files_folder, 'users_avatars',req.params.photo);
      if (fs.existsSync(filePath)) {
        res.sendFile (path.resolve(filePath));
      }
      else{
        res.sendFile(path.resolve('dist/files/ic_tag_faces_black_24dp_2x.png'));
      }
    },

  }
}




