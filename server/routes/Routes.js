var Routes = require('../services/Routes');
var validator = require ('./coocieValidete');


module.exports = cp=>{
  return {
    get:(req, res, next)=>{
      if(req.signedCookies && req.signedCookies.UserDao && req.signedCookies.UserDao){
        let coockie = JSON.parse(req.signedCookies.UserDao);
        let data = {login:coockie.UserLogin ,pass:coockie.pass};
        Routes.get(data,cp)
          .then(data_=>{
            res.status(200).json(data_);
          })
          .catch(err=>{
              res.status(err==="empty"?502:501).send(err);
          })
      }
      else{
        res.status(401).send('coockies not found'); 
      }
    },
    post:(req, res, next)=>{
      if(req.body && req.body.user && req.body.pass){
        let data = {login:req.body.user ,pass:req.body.pass};
        Routes.get(data,cp)
          .then(data_=>{
            res.status(200).json(data_);
          })
          .catch(err=>{
              if(err=='empty'){
                res.status(200).json([]);
              }
              else{
                 res.status(501).send(err||err.message);
              }
          })
      }
      else{
        res.status(401).send('coockies not found'); 
      }
    }, 
    getBYUser:(req, res, next)=>{
       validator(req, res);
        const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
        const {usr} = req.params;
        if(!usr){
          res.status(400).send('Пустой запрос'); 
        }
        Routes.getBYUser({login,pass,usr},cp)
          .then(data_=>{
            res.status(200).json(data_);
          })
          .catch(err=>{
              res.status(err==="empty"?502:501).send(err);
          })
    },
    dellRouteByID:(req, res, next)=>{
        validator(req, res);
        const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
        const {id} = req.params;
        if(!parseInt(id)  ){
          res.status(400).send('Пустой запрос'); 
        }
        Routes.dellRouteByID({login,pass,id},cp)
          .then(data_=>{
            res.status(200).json("SUCCES");
          })
          .catch(err=>{
              res.status(err==="empty"?502:501).send(err);
          })
    },
    postRoute:(req, res, next)=>{
        validator(req, res);
        const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
        const {usr :user} = req.params;
        const {tt,date} = req.body;
        if(!parseInt(tt) || !user || !date ){
          res.status(400).send('Пустой запрос'); 
        }
        Routes.postRoute({login,pass,user,tt,date},cp)
          .then(data_=>{
             res.status(200).json(data_);
          })
          .catch(err=>{
              res.status(err==="empty"?502:501).send(err);
          })
    },
    getRouteByIDAndUsr:(req, res, next)=>{
        validator(req, res);
        const {UserLogin :login,pass} = JSON.parse(req.signedCookies.UserDao);
        const {usr,tt} = req.params;
        if(!parseInt(tt) || !usr  ){
          res.status(400).send('Пустой запрос'); 
        }
        Routes.getRouteByIDAndUsr({login,pass,user,tt,date},cp)
          .then(data_=>{
             res.status(200).json(data_);
          })
          .catch(err=>{
              res.status(err==="empty"?502:501).send(err);
          })
    },
    
  }
}