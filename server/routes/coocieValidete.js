module.exports = (req,res) =>{
	const coocie_st = req.signedCookies && req.signedCookies.UserDao ;
	if(!coocie_st) return notsigned(res);
	const coocie = JSON.parse (coocie_st);
	if(!coocie) return notsigned(res);
	const {UserLogin, pass, ID} = coocie;
	if(!UserLogin || !pass || !ID) return notsigned(res);
}
const notsigned = res=>{
	res.status(401).send('coockies not found'); 
}