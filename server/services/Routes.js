
var Routes = function() {};
var sql = require('mssql');

// Routes.prototype.init = function (data, cp){
// 	return new Promise ((resolve, reject)=>{
// 		this.exec(data, cp)
// 			.then(data=>{
// 				resolve(data);
// 			})
// 			.catch(err=>{
// 				reject(err);
// 			})

// 	})
// };


Routes.prototype.get = function (data, cp){
	return new Promise((resolve,reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.execute('web.GET_Routes')
			.then(data_ =>{
				if(!data_[0].length){
		        	reject('empty');
		        }
		        else{
		        	resolve(data_[0]);
		        }
			}).
			catch(errr=>{
				reject(errr);
			});

	})
	
};

Routes.prototype.getBYUser = function (data, cp){
	return new Promise((resolve,reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('usr', sql.NVarChar(50), data.usr);
		request.execute('web.GET_ROUTES_ADM')
			.then(data_ =>{
		        if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0]);
			}).
			catch(errr=>{
				reject(err.message ? err.message : err);
			});
	})
};

Routes.prototype.dellRouteByID = function (data, cp){
	return new Promise((resolve,reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('id', sql.Int, data.id);
		request.execute('web.DELL_ROUTE_ADM')
			.then(data_ =>{
		        if(data_ && data_[0] && data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve();
			}).
			catch(err=>{
				reject(err.message ? err.message : err);
			});
	})
};

Routes.prototype.getRouteByIDAndUsr = function (data, cp){
	return new Promise((resolve,reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('usr', sql.NVarChar(50), data.usr);
		request.input('tt', sql.Int, data.tt);
		request.execute('web.GET_ROUTES_BYTT_ADM')
			.then(data_ =>{
		        if(data_ && data_[0] && data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0]);
			}).
			catch(err=>{
				reject(err.message ? err.message : err);
			});
	})
};

const stringToMSSQL = (date)=>{
	const DATE = new Date(date);
	return `${DATE.getYear()+1900}-${
				DATE.getMonth()+1<10?'0':''}${DATE.getMonth()+1}-${
				DATE.getDate()<10?'0':''}${DATE.getDate()}`
}

Routes.prototype.postRoute = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(80), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('user', sql.NVarChar(80), data.user);
	    request.input('tt', sql.Int, data.tt);
	    request.input('date', sql.NVarChar(80), stringToMSSQL(data.date));
	    request.execute('web.PUT_ROUTE')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0] && data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
		
	})
};

module.exports = new Routes();