
var MyUsers = function() {};
var sql = require('mssql');
const nodemailer = require('nodemailer');
const {mail,files_folder} = require('../../properties.js');
var fs = require('fs');
var path = require("path");

MyUsers.prototype.get = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
	    request.input('pass', sql.NVarChar(50), data.pass);
	    request.input('ID', sql.Int, data.id);
	    request.execute('web.GET_MyUsers')
	    	.then(data_=> {
	        	resolve(data_[0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};

MyUsers.prototype.post = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('ADM_LOGIN', sql.NVarChar(80), data.c_login);
		request.input('ADM_Pass', sql.NVarChar(50), data.c_pass);
		request.input('UserLogin', sql.NVarChar(80), data.login);
	    request.input('UserPass', sql.NVarChar(50), data.pass);
	    request.input('FirstName', sql.NVarChar(100), data.FirstName);
	    request.input('LastName', sql.NVarChar(100), data.LastName);
	    request.input('Email', sql.NVarChar(50), data.mail);
	    request.input('BuyerID', sql.Int, data.id);
	    request.execute('web.PUT_USER')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	    		sendMail(data_[0][0], data.host);
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
		
	})
};

MyUsers.prototype.put = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('ADM_LOGIN', sql.NVarChar(80), data.c_login);
		request.input('ADM_Pass', sql.NVarChar(50), data.c_pass);
		request.input('ID', sql.Int, data.id);
		request.input('UserLogin', sql.NVarChar(80), data.login);
	    request.input('UserPass', sql.VarChar(50), data.pass);
	    request.input('FirstName', sql.NVarChar(100), data.FirstName);
	    request.input('LastName', sql.NVarChar(100), data.LastName);
	    request.input('Email', sql.NVarChar(50), data.mail);
	    request.execute('web.UPDATE_USER')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};

MyUsers.prototype.putAvatar= function (data, cp){
	return new Promise((resolve, reject)=>{
		saveImage(data.login, data.avatar, data.photo)
			.then(avatar=>{
				return putAvatar (data,avatar, cp)
			})
			.then(userNewAvatar=>{
				resolve(userNewAvatar);
			})
			.catch (err=>{
				console.log(err);
				reject(err.message ? err.message : err);
			})
	})
};

MyUsers.prototype.dell= function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('ADM_LOGIN', sql.NVarChar(80), data.c_login);
		request.input('ADM_Pass', sql.NVarChar(50), data.c_pass);
		request.input('ID', sql.Int, data.id);
		request.input('Active', sql.Int, data.act);
	    request.execute('web.UPDATE_USER_ACTIVITY')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};




const saveImage = (userName, avatar, photo)=>{
	return new Promise((resolve,reject)=>{
		const imgFolder = path.join(files_folder,'users_avatars');
		if (!fs.existsSync(imgFolder)) {
			fs.mkdirSync(imgFolder);
    	}
    	const dateStamp = new Date().valueOf();
    	const avatarFile = `${userName+dateStamp}.jpeg`;
    	const photoFile  = `photo_${userName+dateStamp}.jpeg`;
    	WriteFile(path.join(imgFolder, avatarFile), dataToBase64(avatar))
    		.then(()=>{
    			if(photo) return WriteFile(path.join(imgFolder, photoFile), dataToBase64(photo));
    			return new Promise((resolve,reject)=>{resolve()})
    		})
    		.then (()=>{
    			resolve(avatarFile);
    		})
    		.catch(err=>{
    			console.log(err)
    			reject(err.message ? err.message : err);
    		})
	})
}

const dataToBase64 =data=>{
	return new Buffer(data.replace(/^data:image\/jpeg;base64,/, "").replace(/^data:image\/png;base64,/, ""), 'base64'); 
}

const WriteFile =(path_, buf)=>{
	return new Promise ((resolve,reject)=>{
		fs.writeFile(path_, buf, function(err) {
			if(err) {
				console.log("err", err);
				reject(err && err.message);
			} 
			else {
				resolve('succes');
			}
		})
	})
}

const putAvatar =  (data,avatar, cp)=>{
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('ADM_LOGIN', sql.NVarChar(80), data.c_login);
		request.input('ADM_Pass', sql.NVarChar(50), data.c_pass);
		request.input('ID', sql.Int, data.id);
		request.input('Avatar', sql.NVarChar(200), avatar);
	    request.execute('web.UPDATE_USER_AVATAR')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};







const sendMail = (user = {},host)=> {
	let transporter = nodemailer.createTransport(mail);
	let mailOptions = {
	    from: '"bi-serv 📲" <info@bi-serm.com>', // sender address
	    to: user.mail, // list of receivers
	    subject: 'Данные для входа в DMS систему', // Subject line
	    text: '', // plain text body
	    html: 
	    `<div style = "width :100%;">
			<div style = "width :100%; text-align: center;">
				<img 
					src = "https://lh3.googleusercontent.com/WZBswlJIp33xNtIuFrkYbRemtqdlg5VSVnPa2vKxpMW1tfOeqAFEYTj-8bXIrjFDKkYXjdGN4GAKxWC4p0sIusnWtd2-OdIvLtjgOGWabN6wpIo45XHv9Ld89fiAI_4Afo-05GNYNgdJTk3qAL0l0rWLGay7dh-EJlA8tn_rTz98bYIuoizK-kJDNQl4rT7VAsfPsMB8awYeASyjyr30k6GWyejdozWY-g-O0p4YrqImPfnD2qpzMQYlfovvw7EZp6LBnjzJiHYRiEUFntrlJdIait8bbY323pOE3Yc3OIM9mZa0qLiFVujxVkGaREVii9R3Bi2Aq2Vle44Pd8nXQ8tFj9OvYF2IQksyRFC11mhV-LaGnLD-MFKVigx3QJQT0K4d3kjjCkMdV_psEkT96zmVMM8wg7QKMqVrBgqPDMPNufgOtKYOB79zNUoqiCuGmjNGfVW0C60zmvKLbB0oHnjGRVijmKwd7CPSA6nVSU5bbKZkYSESA3bzfn1kZ56OWPEozDIwGO_1MAe2dULUSglEg-6wgsBupdjErwn44WfiXM0EnatT4Fxgz8J7pd-e03IyxyqD=w1280-h703"
					style = "width: 80%; max-width: 400px; height: auto;"
				/>
				<h1 style ="color: #394246; padding: 10px 0px" >Здравствуйте</h1>
			</div>
			<p style="display: block; padding-left: 10px; font-size: 18px" >
				Данные для входа в DMS систему:
			</p>
			<p style="display: block; padding-left: 10px; font-size: 18px" >
				Логин: ${user.login}
			</p>
			<p style="display: block; padding-left: 10px; font-size: 18px" >
				Пароль: ${user.pass}
			</p>
			<p style="display: block; padding-left: 10px; font-size: 18px" >
				Для входа в систему поситите пройдите по <a href="https://${host}/">ссылке</a>
			</p>
		</div>`
	};
	transporter.sendMail(mailOptions, (error, info) => {
	    if (error) {
	        return console.log(error);
	    }
	    console.log('Message %s sent: %s', info.messageId, info.response);
	});
}


module.exports = new MyUsers();