
var UsersTT = function() {};
var sql = require('mssql');


UsersTT.prototype.get = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
	    request.input('pass', sql.NVarChar(50), data.pass);
	    request.execute('web.GET_USERS_TT')
	    	.then(data_=> {
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};

UsersTT.prototype.post = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(80), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('usr', sql.NVarChar(80), data.usr);
	    request.input('tt', sql.Int, data.tt);
	    request.execute('web.PUT_USERS_TT')
	    	.then(data_=> {
	    		if(!data_[0] || ! data_[0][0]){
	    			reject("SERVER ERROR 0964");
	    		}
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0][0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
		
	})
};


UsersTT.prototype.dell= function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(80), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('id', sql.Int, data.id);
	    request.execute('web.DELL_USERS_TT')
	    	.then(data_=> {
	    		if(data_[0] && data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve('succes');
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};

UsersTT.prototype.put= function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(80), data.login);
		request.input('pass', sql.NVarChar(50), data.pass);
		request.input('id', sql.Int, data.id);
		request.input('period', sql.NVarChar(80), data.period);
	    request.execute('web.UPD_Periodycity')
	    	.then(data_=> {
	    		if(data_[0] && data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve('succes');
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};









module.exports = new UsersTT();