
var TT = function() {};
var sql = require('mssql');


TT.prototype.get = function (data, cp){
	return new Promise((resolve, reject)=>{
		var request = new sql.Request(cp);
		request.input('login', sql.NVarChar(50), data.login);
	    request.input('pass', sql.NVarChar(50), data.pass);
	    request.execute('web.GET_TT')
	    	.then(data_=> {
	    		if(data_[0][0] && data_[0][0].ERROR){
	    			reject(data_[0][0].ERROR);
	    		}
	        	resolve(data_[0]);
	    	})
	    	.catch(err=> {
		       reject(err.message ? err.message : err);
		    });
	})
};










module.exports = new TT();