
var Routes_Rout = function (cp){
  const router = require('express').Router();
  const dbrouters = require('../routes/Routes')(cp);
 
  router.get('/', dbrouters.get);
  router.post('/', dbrouters.post);
  router.get('/:usr', dbrouters.getBYUser);
  router.delete('/:usr/:id', dbrouters.dellRouteByID);
  router.post('/:usr', dbrouters.postRoute);
  router.get('/:usr/:tt', dbrouters.getRouteByIDAndUsr);
  return router;
}




 module.exports = Routes_Rout;