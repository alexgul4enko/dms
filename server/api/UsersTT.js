

var rout = function (cp){
  const router = require('express').Router();
  const dbrouters = require('../routes/UsersTT')(cp);
  router.get('/', dbrouters.get);
  router.post('/', dbrouters.post);
  router.delete('/:id', dbrouters.del);
  router.put('/:id', dbrouters.put);
  return router;
}




 module.exports = rout;