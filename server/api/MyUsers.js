

var rout = function (cp){
  const router = require('express').Router();
  const dbrouters = require('../routes/MyUsers')(cp);
  router.get('/', dbrouters.get);
  router.post('/', dbrouters.post);
  router.put('/:id', dbrouters.put);
  router.delete('/:id', dbrouters.del);
  router.put('/:id/avatar', dbrouters.putAvatar);
  router.get('/avatar/:photo', dbrouters.getImage);
  return router;
}




 module.exports = rout;