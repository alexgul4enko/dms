import React, { Component ,PropTypes} from 'react';
import './select.css'
export default class Select extends Component{
	constructor(props){
		super(props);
		this.state={
			open:false,
			filter:'',
		}
		this.handleClick = this.handleClick.bind(this);
		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
		this.filter = this.filter.bind(this);
	}

	render(){
		return  <div style = {styles.main(this.props.width, this.props.height)}>
					<div style = {styles.wrapper}>
						{this.renderIcon(this.props.icon)}
						{this.renderTitle.call(this)}
					</div>
					<ul style = {styles.ul(this.props.height,this.props.icon)}>
						{this.renderdata.call(this)}
					</ul>
				</div>
	}
	renderIcon(icon){
		return icon? <div className = "material-icons" style = {styles.icon}>{icon}</div> : null
	}
	renderTitle(){
		return this.state.open ?
					(<input 
						className = "search-data"
						style = {styles.seach(this.props.icon)}
						onChange = {this.filter}
						type="search" 
						autoFocus
						onKeyDown = {this.handleKey.bind(this)}
					/>):
					(<span 
						onClick = {this.open} 
						style = {styles.title(this.props.icon)}
					>
						{this.props.value||this.props.placeHolder}
					</span>)


	}

	renderdata(){
		return this.state.open ?
					this.props.data.map((item,i)=>
						(<DropDownItems
							handleClick = {this.select.bind(this,item)}
							{...this.props.parseData(item,i)}
							filter = {this.state.filter}
						/>)):null;
	}
	filter(e){
		this.setState({filter:e.target.value});
	}
	select(val){
		this.close();
		this.props.onChange(val);
		this.setState({value:val});
	}

	handleClick(e){
		const targetClass = e.target.className;
		if(!targetClass.includes("hover-opacity") && !targetClass.includes("search-data")){
			this.close();
			e.preventDefault();
			e.stopImmediatePropagation();
		}
	}
	handleKey(e){
		if(e.which == 27){
			this.close();
		}
	}
	open() {
		console.log('open')
		this.setState({open:true,filter:''});
		document.addEventListener('click',this.handleClick)
	}
	close() {
		this.setState({open:false,filter:''});
		document.removeEventListener('click',this.handleClick)
	}

}

Select.defaultProps={
	parseData:(item,i)=>({key:item+i, title:item}),
	placeHolder:"Select one:",
	icon:''
}

Select.propTypes={
	value:PropTypes.string,
	data:PropTypes.array,
	onChange: PropTypes.func.isRequired,
	parseData:  PropTypes.func,
	placeHolder: PropTypes.string,
	icon:PropTypes.string
}



const DropDownItems = props =>(
	filt(props.title, props.filter) ? 
			<div className = "hover-opacity"
				style = {styles.li} 
				onClick= {props.handleClick}
			>
				{props.title}
			</div> : null
)

DropDownItems.propTypes = {
	handleClick:PropTypes.func.isRequired,
	title: PropTypes.string,
	filter: PropTypes.string,
}


const filt  =(item='', filter)=>{
	if(!filter) return true;
	return item.toLowerCase().includes(filter.toLowerCase()) 
}



const styles={
	main:(width = "100%", height ="30px")=>({
		width: width,
		height: height,
		paddingBottom: "2px",
		borderBottom:'1px solid #394246',
		textAlign:'center',
		lineHeight:height,
		color:'#394246',
		fontSize:'1.4em',
		cursor:'pointer',
		position:'relative'
	}),
	title:(i=false)=>({
		width: i ? "calc(100% - 50px" : "100%",
		height: '100%',
		display:'block',
		color:"#394246",
		fontSize: "1.4em",
		textAlign:'center'
	}),
	ul:(top = 30,i=false)=>({
		position:'absolute',
		width: i ? "calc(100% - 50px" : "100%",
		zIndex:6,
		right:0,
		top: `${top+7}px`,
		maxHeight:150,
		overflowY: 'auto',
		margin:0,
		padding:0,
	}),
	li:{
		background:"#394246",
		width:'100%',
		height:28,
		opacity:0.8,
		color:'white',
		cursor:'pointer',
		lineHeight: "28px"
	},
	seach:(i=false)=>({
		background:"transparent",
		height: '100%',
		width: i ? "calc(100% - 50px" : "100%",
		border:0,
		color:"#394246",
		fontSize: "1.1em",
		textAlign:'center'
	}),
	wrapper:{
		width:"100%",
		height:"100%",
		display:"flex",
		flexDirection:'row',
		justifyContent:'space-between'

	},
	icon:{
		width:50,
		textAlign:'center',
		fontSize:"2.5em"
	}

}
