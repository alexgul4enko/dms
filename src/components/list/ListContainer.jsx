import React , {Component} from 'react';
import List from './List';
import ListHeader from './ListHeader';
import './list.css';
import textfilter from './textFilter'

export default class UsersList extends Component {
	render (){
		return (
			<div className = 'FiltListContainer'>
				{this.props.children}
			</div>
		)
	}
}

export {List, ListHeader,textfilter};

