import React , {Component, PropTypes,Children } from 'react';
import uuid from 'uuid';
export default class List extends Component {
	constructor(props){
		super(props);
		this.state = {
			filter:'',
			to: props.batch

		}
		this.renderFilter = this.renderFilter.bind(this);
		this.filter = this.filter.bind(this);
		this.renderList = this.renderList.bind(this);
		this.onScroll = this.onScroll.bind(this);
		
	}
	render(){
		return(
			<div className = {`FiltList ${this.props.class}`}>
				{
					this.renderFilter()
				}
				<div className = "FiltListContent" id = {this.listId}>
					{this.renderList()}
				</div>

			</div>
		)
	}
	renderList(){
		if (!this.state.filter){
			if(this.props.items.length > this.state.to){
				return this.props.items.slice(0, this.state.to)
								.map((item,key)=>this.props.renderItem(item,key));
			}	
			return this.props.items.map((item,key)=>this.props.renderItem(item,key));
		}
		else{
			const filtered = this.props.items.filter(item=>{
					return this.props.filter(item, this.state.filter);
				});
			if(filtered.length > this.state.to){
				return filtered.slice(0, this.state.to)
								.map((item,key)=>this.props.renderItem(item,key));
			}
				return filtered.map((item,key)=>this.props.renderItem(item,key));
		}
	}
	renderFilter(){
		if(!this.props.filter) return null;
		return (
				<input 
					type = "search"
					defaultValue = {this.state.filter}
					onChange = {this.filter}
					placeholder = {this.props.filterPlaceHolder}
				/>
		)
	}
	filter(e){
		this.setState({filter:e.target.value});
	}

	componentWillMount() {
		this.listId = uuid.v4()
	}
	onScroll(e){
		const {offsetHeight,scrollTop,scrollHeight} = e.target;
		if( scrollTop + offsetHeight >= scrollHeight){
			this.setState({to:this.state.to+this.props.batch});
		}
	}
	componentDidMount() {
		document.getElementById(this.listId).addEventListener('scroll',this.onScroll)
	}

	componentWillUnmount() {
		document.getElementById(this.listId).removeEventListener('scroll',this.onScroll)
	}

}

List.defaultProps = {
	class:'',
	items : [],
	filterPlaceHolder: 'search',
	batch:10
}

List.propTypes = {
	class: PropTypes.string,
	items : PropTypes.array,
	filter: PropTypes.func,
	renderItem: PropTypes.func.isRequired,
	filterPlaceHolder : PropTypes.string,
	batch: PropTypes.number,
}

