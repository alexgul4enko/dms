import React, { PropTypes, Component } from 'react';
import './ImageEditor.css'
import Cropper from 'cropperjs';
import 'cropperjs/dist/cropper.min.css';
import Input from 'react-toolbox/lib/input';

export default class ImageEditor extends Component {
	constructor(props){
		super(props);
		const photo = (props.photo && props.photo !='-')?`/api/MyUsers/avatar/photo_${props.photo}` : null;
		this.state = {
			cropImg : null,
			photo,
			name :'Выберите фото',
	    }
	    this.renderAvatar = this.renderAvatar.bind(this);
	    this.getPhotos = this.getPhotos.bind(this);
	}

	render(){
		return (
			<div className = "imageEditor">
				<div className = "input-wrapper">
				<span className = 'material-icons'>add_a_photo</span>
				<span className = 'file-name'>{this.state.name}</span>
				<input
					defaultValue = {this.state.name}
					type='file'
					id = "ImageEditorInputFile"
					className = "avatar-input"
					accept="image/jpeg"
					onChange={this.handleChange.bind(this)}
				/>
				</div>

				<div className = 'wrapper'>
					<div style = {styles.container}>
						<img id="image_crrr" src={this.state.photo} style = {styles.image}/>
					</div>
					{this.renderAvatar()}
				</div>
			</div>
		)
		
	}
	renderAvatar(){
   		return (this.state.cropImg || this.state.photo) ?
   				<img 
						width={100}
						height = {100}
						src = {this.state.cropImg ? this.state.cropImg : null}
						style = {{borderRadius:"50%"}}
					/> 
				: null;
	}

	handleChange(e){
		e.preventDefault();
   		const file = e.target.files[0];
   		const reader = new FileReader();
   		if(file.size > 102400 ){
   			this.props.showError("Макс. размер файла - 100kb");
   			e.preventDefault();
   			// e.stopImmediatePropagation();
   			return;
   		}
   		reader.readAsDataURL(file);
   		reader.onload = e => {
   			this.setState({photo:e.target.result,cropImg : null, name:file.name});
   			this.cropper.replace(this.state.photo);
   		}
	}

	componentDidMount() {
		const image = document.getElementById('image_crrr');
		this.cropper = new Cropper(image, {
			aspectRatio: NaN,
			viewMode:2,
			dragMode:'crop',
			scalable:false,
			zoomable:false,
			crop: () => {
        		const cropImg = this.cropper.getCroppedCanvas().toDataURL();
        			this.setState({cropImg});
			}
		});

	}

	componentWillUnmount() {
		this.cropper.destroy();
	}
	getPhotos() {
		const {cropImg :avatar,photo} = this.state;
		return {avatar,photo};
	}
}


ImageEditor.propTypes ={

}

const styles = {
  image: {
    maxWidth:200,
  },
  container:{
    width:300,
    maxHeight:400,

  }
}


	
