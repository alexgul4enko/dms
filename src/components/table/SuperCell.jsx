import React, { Component, PropTypes } from 'react';


export default class SupperCell extends Component{
  constructor(props){
    super(props);
    this.state = {
      active:false
    }
    this.click = this.click.bind(this);
    this.renderCell =this.renderCell.bind(this);
    this.renderSuperCell = this.renderSuperCell.bind(this);
  }
  render(){
    return this.props.isSuper ? this.renderSuperCell() : this.renderCell();
  }
  click(){
    this.setState({active:true});
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.isSuper !== this.props.isSuper){
      this.setState({active:false});
    }
  }
  renderCell(){
    const {data,columnKey,rownum} = this.props
    return (
      <div 
        className='BICell'
        data-columnKey={columnKey}
        data-rownum={rownum}
      >
        {this.state.active ? this.props.editData() : data}
      </div>
    )
  }
  renderSuperCell(){
    const {data,columnKey,rownum} = this.props
    return (
      <div 
        className='BICell Selected'
        onClick={this.handleClick.bind(this)}
        data-columnKey={columnKey}
        data-rownum={rownum}
      >
        {this.state.active ? this.props.editData() : data}
      </div>
    )
  }
  handleClick(){
    this.setState({active:true});
  }

}

SupperCell.propTypes = {
  data:PropTypes.string,
  editData:PropTypes.func,
  isSuper:PropTypes.bool
}

SupperCell.defaultProps = {
  data:'',
  isSuper:false
}