import React , { Component, PropTypes} from 'react';




export default class ColumnsGroupHeader extends Component{
	constructor(props){
		super (props);
		this.state = {
			tongle:false,
		}
		this.renderTongle = this.renderTongle.bind(this);
		this.tongle = this.tongle.bind(this);
	}
	render(){
		return (
			<div className = {`GroupHeader ${this.props.className}`} >
				{this.state.tongle ? '':this.props.title}
				{this.renderTongle()}
			</div> 
		)
	}

	renderTongle(){
		return this.props.title ? 
					(<span 
						className = 'GroupHeader_TONGLE'
						onClick = {this.tongle}
						>
						{this.state.tongle ? '+':'-'}
					</span>) : ''
	}

	tongle(){
		const {tongle }= this.state;
		this.props.tongleGroup(!tongle);
		this.setState({tongle: !tongle});
	}
}

ColumnsGroupHeader.defaultProps = {
	className:'',
}

ColumnsGroupHeader.propTypes = {
	title: PropTypes.string.isRequired,
	tongleGroup : PropTypes.func.isRequired,
	className : PropTypes.string,
}
