import React , {Component, PropTypes} from 'react';
import 'react-dates/lib/css/_datepicker.css';
import { SingleDatePicker ,DateRangePicker} from 'react-dates';
import uuid from 'node-uuid'; 
import moment from 'moment'

class CalendarIcon extends React.Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	focusedInput: null,
	    	startDate: null,
	    	endDate: null,
	    };
	    this.initialVisibleMonth = this.initialVisibleMonth.bind(this);
		this.onDatesChange = this.onDatesChange.bind(this);
		this.onFocusChange = this.onFocusChange.bind(this);
	}

	onDatesChange({ startDate, endDate }) {
		this.setState({ startDate, endDate });
		this.props.changeFilter(startDate, endDate);
	}

	onFocusChange(focusedInput) {
		this.setState({ focusedInput });
	}


	initialVisibleMonth(){
		return this.state.startDate || this.state.endDate  || moment();
	}
	render() {
		moment.locale('ru');
		const { focusedInput, startDate, endDate } = this.state;
		return (
			<div>
				<DateRangePicker
					{...this.props}
					onDatesChange={this.onDatesChange}
					onFocusChange={this.onFocusChange}
					focusedInput={focusedInput}
					startDate={startDate}
					endDate={endDate}
					startDatePlaceholderText = "c"
					endDatePlaceholderText = "до"
					withPortal = {true}
					showClearDates = {true}
					customArrowIcon = {<span>&#x2192;</span>}
					minimumNights = {0}
					startDateId = {uuid.v4()}
					endDateId = {uuid.v4()}
					displayFormat = "DD/MM/YY"
					monthFormat = "MMM YYYY"
					isOutsideRange = {()=>false}
					initialVisibleMonth={this.initialVisibleMonth}
				/>
			</div>
		);
	}
}


CalendarIcon.propTypes = {
	changeFilter:PropTypes.func.isRequired,
}

export default CalendarIcon;