export default function filterText(filter, value){
	if (!value) return false;
	if(!filter) return true;
	const f = filter.toLowerCase(),
		  v = value.toLowerCase();
	
	const conditions = f.split('|').map(item=>(item.split('*')));
  return checkORS(conditions,v);  
}


const checkORS=(ors=[],v)=>{
  return ors.filter(or=>(checkANDS(or,v))).length>0;
}
const checkANDS = (ands = [],v)=>{
  const l = ands.length;
  for(let i =0; i<l ; i++){
    if(!checkValue(ands[i],v)){
      return false;
    }
  }
  return true;
}


const checkValue =(f='',v='')=>{
  if(f.startsWith("^") ){
    return v.startsWith(f.substr(1));
  }
  else if (f.endsWith("^")){
    return v.endsWith(f.substr(0,f.length-1))
  }
  else if(f.startsWith("=") ){
   return v == f.substr(1);
  }
  else{
    return v.includes(f);
  }
}