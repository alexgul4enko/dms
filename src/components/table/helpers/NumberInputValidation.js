export default function NumberInputValidation (value = ''){
	if (!value) return '';
	
	const cleanVal = value.replace(/[^0-9.><=|*-]+/,'');
	if (!cleanVal) return '';
	//filter cant start from . * |
	if(value == '|' || value == '*' || value == '.') return '';
	return cleanVal.split('').reduce((prev,curr)=>{
		if (!prev) return [curr];
		const last = prev[prev.length-1];
		if( validateTyping(last, curr)){
			if(curr=='.' && !validateDot(prev)){
				return prev;
			}
			return [...prev, curr]
		}
		return prev;
	},null).join('');
}





const validateTyping=(prev, cur)=>{
	switch(prev){
		case String(prev.match(/\d/)):
			switch(cur){
				case '<':return false;
				case '>':return false;
				case '=':return false;
				case '-':return false;
				default :return true;
			}
			break;
		case '.':
			if(cur.match(/\d/)) return true;
			return false;
		case '-':
			if(cur.match(/\d/)) return true;
			return false;
		case '>':
			if(cur.match(/\d/) || cur=='='|| cur=='-') return true;
			return false;
		case '<':
			if(cur.match(/\d/) || cur=='='|| cur=='-') return true;
			return false;
		case '=':
			if(cur.match(/\d/) || cur=='-') return true;
			return false;
		case '*':
			switch(cur){
				case '|':return true;
				case '*':return true;
				case '.':return true;
				default :return true;
			}
			break;
		case '|':
			switch(cur){
				case '|':return false;
				case '*':return false;
				case '.':return false;
				default :return true;
			}
			break;
	}
}

const validateDot = (prev = [])=>{
	for (let i = prev.length; i>=0;i--){
		const char = prev[i];
		if(char == '.') return false;
		if(char =='|' || char =='*' ) return true;
	}
	return true;
}