import Constances from '../Constances';

export default function mover (direction, row,col, co_rows, co_cols){
	switch (direction){
		case Constances.UP:
			if(row ==0) return false;
			return {row:row-1,col};
		case Constances.DOWN:
			if(row+1 == co_rows)return false;
			return {row:row+1,col};
		case Constances.LEFT:
			if(col == 0) return false;
			return {row, col:col-1};
		case Constances.RIGHT:
			if(col+1 == co_cols)return false;
			return {row, col:col+1};
		case Constances.TAB:
			if(col+1 == co_cols)return false;
			return {row, col:col+1};
		default:
			return false;
	}
}