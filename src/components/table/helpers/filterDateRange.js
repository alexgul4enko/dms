import moment from 'moment';
export default function filterDateRange (from, to, date){
	const date_m = date ? moment(date, 'YYYY-MM-DD'): null;
	const date_st = date_m ? date_m.format('YYYY-MM-DD') : '2000-01-01';
	const from_st = from ?  from.format('YYYY-MM-DD') : '1990-01-01';
	const to_st = to ?  to.format('YYYY-MM-DD') : '2100-01-01';

	return moment(date_st).isSameOrAfter(from_st) && 
			moment(date_st).isSameOrBefore(to_st) ;
	
}