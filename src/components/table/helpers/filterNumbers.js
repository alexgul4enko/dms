export default function filterNumbers (f,v){
	if (!v) return false;
	if(!f) return true;
	const val = parseNumber(v);
    if(isNaN(val)) return false;
	const conditions = f.split('|').map(item=>(item.split('*')));
  	return checkORS(conditions,val);

}


const checkORS=(ors=[],v)=>{
  return ors.filter(or=>(checkANDS(or,v))).length>0;
}


const checkANDS = (ands = [],v)=>{
  const l = ands.length;
  for(let i =0; i<l ; i++){
    if(!checkValue(ands[i],v)){
      return false;
    }
  }
  return true;
}


const checkValue =(filt='',v)=>{
  const condition = filt.replace(/[0-9.-]+/,'') ||'=';
  const f =  parseNumber(filt.replace(/[>=<]+/,''));
  switch(condition){
    case '=':return v == f;
    case '>':return v>f;
    case '<':return v<f;
    case '>=':return v>=f;
    case '<=':return v<=f;
  }
  return false;
}


const parseNumber = val=>{
	return parseFloat(val);
}