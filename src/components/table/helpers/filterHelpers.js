export function updateFilterDatByKey(arr=[], key,val){
	return arr.map(item=>{
		if(item.key == key)return {...item,val};
		return item;
	})
}


export function deleteFilterDatByKey(arr=[], key){
	return arr.filter(item=>(item.key != key));
}

export function getFilterDatByKey(arr=[], key){
	return arr.filter(item=>(item.key == key))[0]||null;
}