export default function sorter(data=[],col,indexes=[]){
	return [...indexes].sort(sortByColumn(col,data));
}


const sortByColumn=(col,data)=>{
	return (a,b)=>{
		const {[col]: a_col} = data[a];
		const {[col]: b_col} = data[b];
		if(a_col<b_col) return -1;
		if(a_col>b_col) return 1;
		return 0;
	}
}