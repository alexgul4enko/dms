import filterText from './filterText';
import filterNumbers from './filterNumbers';
import filterDateRange  from './filterDateRange';

export default function tableFilter(data= [], filters =[],indexes=[]){
	const fl = filters.length;
	if (!fl) return indexes;
	return indexes.filter(key=>checkFiter(fl,filters,data[key]));
}

const checkFiter = (fl = 0, filters =[], item={})=>{
	if(!fl) return true;
	for(let i =0; i<fl;i++){
		const {key, val,type} = filters[i];
		switch(type){
			case 'text':
				if(!filterText(val,item[key])) return false;
				break;
			case 'numeric':
				const chekItem = parseNumber(item[key]);
				if(isNaN_(chekItem)) return false;
				if(!filterNumbers(val,chekItem)) return false;
				break;
			case 'date':
				const {endDate, startDate} = val;
				if(!endDate && !startDate ) return true;
				if(!filterDateRange(startDate,endDate, item[key])) return false;
				break;	

		}
	
	}
	return true;
}


const parseNumber = val=>{
	return parseFloat(val);
}

const isNaN_  = val=>{
	return isNaN(val);
}