import React , {Component, PropTypes} from 'react';
import 'fixed-data-table/dist/fixed-data-table.css';
import  {Table, Column, Cell, ColumnGroup} from 'fixed-data-table';
import './table.css';
import uuid from 'node-uuid'; 
import TableFooter from './TableFooter';
import ColumnHeader from './ColumnHeader';
import tableFilter from './helpers/tableFilter';
import ColumnsGroupHeader from './ColumnsGroupHeader';
import {updateFilterDatByKey,deleteFilterDatByKey,getFilterDatByKey} from './helpers/filterHelpers';
import sorter from './helpers/sorter';
import SuperCell from './SuperCell';
import mover from './helpers/Mover';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import SupperCell from './SuperCell';


export default class TableComponent extends Component{
	constructor(props){
		super(props);
		this.state = {
			filters:[],
			filerIndexes: [],
			indexes : [],
			columnsLength:{},
			sort: null,
			from: null,
			to:null,
			superCell:null
		}
		this.renderColumns = this.renderColumns.bind(this);
		this.renderCell = this.renderCell.bind(this);
		this.onColumnResizeEndCallback = this.onColumnResizeEndCallback.bind(this);
		this.getDataByIndex = this.getDataByIndex.bind(this);
		this.calculateWindth = this.calculateWindth.bind(this);
		this.calculateTableWidth = this.calculateTableWidth.bind(this);
		this.renderColumnHeader = this.renderColumnHeader.bind(this);
		this.reverseSort = this.reverseSort.bind(this);
		this.moveKell = this.moveKell.bind(this);
		this.clearSelection = this.clearSelection.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.editData = this.editData.bind(this);
	}
	render(){
		const {isGroups, columns} = this.gropColumns(this.props.columns);
		const width = this.calculateTableWidth();
		const headerHeight = this.props.changeFontSize ? 30 : 0;
		const scrollColl = this.state.from && this.state.from.col_index || 0;
		const scrollRow= this.state.from && this.state.from.row || 0;
		return (
			<div 
				className = "tableContainer" 
				style = {{width}}
				onClick={this.handleClick}
			>
				{headerHeight ? <TableFooter 
									changeFontSize = {this.props.changeFontSize}
									navClass = {this.props.navClass}
							/> : ""}
				<Table
					groupHeaderHeight={isGroups? this.props.groupHeaderHeight : 0}
				 	headerHeight = {44}
			        rowsCount={this.state.filerIndexes.length}
			        rowHeight={30}
			        width={width}
			        scrollToColumn = {scrollColl}
			        scrollToRow = {scrollRow}
			        maxHeight={this.props.height-headerHeight}
			        isColumnResizing = {false}
			        onColumnResizeEndCallback = {this.onColumnResizeEndCallback}
				>
					{this.renderColumns(columns)}
			    	
				</Table>
			</div>
		)
	}
	calculateTableWidth(){
		const {columnsLength} = this.state;
		const columnWidthSum = this.props.columns.reduce((prev,curr)=>{
			const {group, width,type} = curr;
			const {[group]:gr} = columnsLength;
			if(gr){
				return prev+=24/gr;
			}
			return prev+=(type == 'date') ? width<150 ? 150 : width : width;
		},0)
		return columnWidthSum<this.props.width?  columnWidthSum : this.props.width ;
	}
	componentWillMount() {
		const filerIndexes = this.props.data.map((i,key)=>key);
		const indexes = [...filerIndexes];
		this.colums = columnsToObj(this.props.columns)
		this.setState({filerIndexes,indexes });
	}	
	gropColumns(data=[]){
		let isGroups = false;
		const columns = data.reduce((prev, curr, column_index)=>{
			if(!prev) return  [this.parseColumnData({...curr,column_index})];
			if(curr.group) isGroups=true;
			const last = prev[prev.length-1];
			if(last.group && last.group==curr.group){
				last.columns.push({...curr,column_index})
				return prev;
			}
			else{
				return [...prev, this.parseColumnData({...curr,column_index})];
			}
		},null)
		return {columns,isGroups}
	}
	parseColumnData(props={}){
		const {group } = props;
		return {group, columns:[{...props}]};
	}
	renderColumns(columns_data=[]){
		const isFixedGroup = (item=[])=>{
			let fixed = false;
			item.forEach(item=>{
				if(item && item.isFixed) {
					fixed = true;
					return;
				}
			});
			return fixed;
		}
		return columns_data.map(col=>{
			const {group, columns} = col;
			const fixed = isFixedGroup(columns);
			return (
				<ColumnGroup 
					key = {group+uuid.v4()}
					fixed = {fixed}
					header={this.renderColumnsGroupHeader(group,this.props.groupClass)}
				>
					{this.renderColumninGR(columns,fixed)}
				</ColumnGroup>
			)
		})
	}
	renderColumnsGroupHeader(title='', className=''){
		return   (<ColumnsGroupHeader 
						title = {title}
						tongleGroup = {this.tongleGroup.bind(this,title)}
						className = {className}
				/>);
	}
	renderColumninGR(columns=[],fixed = false){
		return columns.map(column=>{
			const {column_index,dbName,editor,isFixed,readOnly,title,type,width,group} = column;
			const {columnWidth, mincolumnWidth } = this.calculateWindth(type,width,group);
			return   (
				<Column 
					key = {dbName+'_'+uuid.v4()}
					columnKey = {dbName}
					width={columnWidth} 
					minWidth = {mincolumnWidth}
					header = {this.renderColumnHeader(title,readOnly,dbName ,type,dbName)}
					fixed = {fixed}
					cell={props=>this.renderCell(props,column_index)}
					isResizable = {true}
					allowCellsRecycling = {true}
				/>
			) 
		})
	}
	calculateWindth(type,width,group){
		const {[group] : gr} = this.state.columnsLength;
		if(gr){
			return {columnWidth: 24/gr,mincolumnWidth : 24/gr  };
		}
		else{
			const columnWidth = (type == 'date')? width<150 ? 150 : width : width;
			const mincolumnWidth = type == 'date'? 150 : 15;
			return {columnWidth,mincolumnWidth};
		}
	}
	renderColumnHeader(title,isEdit,key,type,dbName){
		return <ColumnHeader 
					data ={title} 
					isEdit = {isEdit}
					type = {type}
					key = {key}
					isSorted = {dbName===this.state.sort}
					filter ={this.filterData.bind(this,key,type)}
					sortByColumn = {this.sortByColumn.bind(this,dbName)}
					reverseSort = {this.reverseSort}
					headerClass = {this.props.headerClass}
					value = {this.state.filters.filter((f)=>f.key==key)[0]	|| ''}
				/>
	}
	sortByColumn(sort){
		this.setState({sort});
		const {indexes: indexes_,filters} = this.state;
		const {data} = this.props;
		const indexes = sorter(data, sort,indexes_);
		if (filters && filters.length ){
			const filerIndexes = tableFilter(data,filters,indexes);
			this.setState({indexes,filerIndexes});
			return;
		}
		this.setState({indexes, filerIndexes:[...indexes] });
	}
	reverseSort(){
		let {indexes ,filerIndexes} = this.state ;
		if(!indexes) indexes=[];
		if(!filerIndexes) filerIndexes=[];
		this.setState({
			indexes : [...indexes].reverse(),
			filerIndexes : [...filerIndexes].reverse()
		});
	}
	renderCell(props, index){
		const {columnKey, rowIndex} = props;
		const {row,coll} = this.state.superCell || {};
		const isSuper = columnKey === coll && row == rowIndex ;
		const rownum = this.state.filerIndexes[rowIndex];
		const data = this.props.data && this.props.data[rownum] && this.props.data[rownum][columnKey];
		const ref = `${columnKey}_${rownum}`
		return (
			<SupperCell 
				data={data+''} 
				ref={ref_=>this[ref] = ref_}
				rowIndex={rownum}
				columnKey={columnKey}
				rownum={rowIndex}
				isSuper={isSuper}
				editData={this.editData}
			/>)
	}
	clearSelection(){
		this.setState({from:null, to:null});
	}
	setFrom (data,e){
		const{columnKey: col, rowIndex:row,index:col_index} = data;
		e.preventDefault();
		e.stopPropagation();
		this.setState({from:{col,row,col_index},to:null});
	}
	moveKell(dirrection, row,coll){
		const co_rows = this.state.filerIndexes && this.state.filerIndexes.length || 0;
		const co_cols = this.props.columns.length;
		const mov = mover(dirrection,row,coll,co_rows,co_cols);
		if(mov){
			this.setState({from:{
							col:this.props.columns[mov.col].dbName,
							row:mov.row,
							col_index : mov.col,
						}});
		}
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.dataID !== this.props.dataID){
			const filerIndexes = nextProps.data.map((i,key)=>key);
			const indexes = [...filerIndexes];
			this.colums = columnsToObj(nextProps.columns);
			this.setState({filerIndexes,indexes, superCell:null });
		}
		if (nextProps.columns.length != this.props.columns.length){
			const filters = this.state.filters.filter(i=>{
				const {key} = i;
				return true;
			})
			this.colums = columnsToObj(nextProps.columns);
			this.setState({filters, superCell:null});
		}
	}
	onColumnResizeEndCallback(width,columnKey){
		this.props.resizeColumn({width,columnKey});
	}
	filterData(key,type, val){
		const {filters,filerIndexes} = this.state;
		const filterData = getFilterDatByKey(filters, key);
		let filters_;
		if(!filterData && val){
			filters_ = [...filters, {key,val,type}];
		}
		else{
			if(!val ){
				filters_ = deleteFilterDatByKey(filters,key,val);
			}
			else{
				const {key:old_key,val:old_val} = filterData;
				filters_ = updateFilterDatByKey(filters,key,val);
			}
		}
		this.setState({
				filters: filters_,
				filerIndexes: tableFilter(this.props.data,filters_,this.state.indexes )
			});
	}
	getDataByIndex(index,j){
		const i = this.state.filerIndexes[index];
		return this.props.data[i][j] || '';
	}
	tongleGroup(group, tongl){
		const {columnsLength} = this.state;
		if(tongl){
			const co = this.props.columns.filter(item=>(item.group == group)).length;
			this.setState({columnsLength: {...columnsLength, [group]:co }});

		}
		else{
			const {[group] : gr ,...oth } = columnsLength;
			this.setState({columnsLength: oth});
		}
	}
	handleClick(event){
		let row = event.target.attributes.getNamedItem("data-rownum");
		let coll = event.target.attributes.getNamedItem("data-columnKey");
		row = row && row.value;
		coll = coll && coll.value;
		if(!coll || !row) {
			this.clickOutside();
			return;
		}
		const collProps = this.colums[coll];
		const superCell = {row,coll};
		this.setState({superCell});
	}
	clickOutside(){
		this.setState({superCell:null});
	}
	editData(){
		return <span>Editing...</span>
	}
}

const columnsToObj = (arr=[])=>{
	return arr.reduce((prev,next)=>{
		const {dbName} = next;
		return {...prev,[dbName]:next}
	},{})
}

TableComponent.defaultProps = {
	data:[],
	dataID:'default',
	groupHeaderHeight:30,
	groupClass:'',
	headerClass: '',
	navClass : '',
	fontSize : 12,
	cellClass: '',
}

TableComponent.propTypes ={
	width: PropTypes.number.isRequired,
	height: PropTypes.number.isRequired,
	data: PropTypes.array.isRequired,
	fontSize: PropTypes.number.isRequired,
	changeFontSize : PropTypes.func,
	columns: PropTypes.array.isRequired,
	groupHeaderHeight : PropTypes.number,
	groupClass : PropTypes.string,
	headerClass : PropTypes.string,
	navClass : PropTypes.string,
	cellClass :  PropTypes.string,
	dataID : PropTypes.string.isRequired,
	editData:PropTypes.string,
}