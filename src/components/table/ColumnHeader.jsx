import React, {Component, PropTypes} from 'react';
import {Cell} from 'fixed-data-table';
import NumberInputValidation from './helpers/NumberInputValidation';
import CalendarIcon from './CalendarIcon';
import moment from 'moment';
export default class ColumnHeader extends Component {
	constructor(props){
		super(props)
		this.state = {
			sortDirection: 'DESC',
		}
		this.renderFilter = this.renderFilter.bind(this);
		this.filterDate = this.filterDate.bind(this);
		this.sort = this.sort.bind(this);
		this.renderNameTag = this.renderNameTag.bind(this);
		this.filterBool = this.filterBool.bind(this);
		
	}
	
	render (){
		return (
			<div className = {`BI_ColumnHeader ${this.props.isEdit ? '':'editable'} ${this.props.headerClass}`} >
				{this.renderNameTag()}
				{this.renderFilter()}			
			</div>
		)
	}

	renderNameTag(){
		const {isSorted} = this.props;
		const {sortDirection}  = this.state;
		const arrow = isSorted ? sortDirection == "DESC" ? (<span>&#x2191;</span>):(<span>&#x2193;</span>):'';
		return (<span 
					onClick = {this.sort}
				>
				{arrow}  {moment.isMoment(this.props.data)?this.props.data.format('dd DD-MM-YY'):this.props.data}
				</span>)
	}
	shouldComponentUpdate(nextProps, nextState) {
		if(!nextProps.isSorted && this.state.sortDirection != 'DESC'){
			nextState.sortDirection = 'DESC';
		}
		return true;
	}
	
	
	renderFilter(){ 
		switch(this.props.type){
			case 'date':
				return (<CalendarIcon  changeFilter= {this.filterDate}/>)
			case 'bool':
				return (<button 
							className = 'material-icons'
							onClick = {this.filterBool}
						>
							{this.props.value && this.props.value.val?
								this.props.value.val ==='true'?
									'check_box':'check_box_outline_blank'
								:"indeterminate_check_box"}
						</button>)
			default :
				return <input 
					type="search" 
					placeholder=""
					value = {this.props.value && this.props.value.val}
					onChange = { this.filter.bind(this, this.props.type)}
				/>
		}
	}



	sort(){
		if(!this.props.isSorted){
			this.props.sortByColumn();
		}
		else{
			const {sortDirection} = this.state;
			const reverse = sortDirection=="DESC" ? "ASC": "DESC" ;
			this.setState({sortDirection:reverse});
			this.props.reverseSort();
		}
	}
	filterBool(){
		const value = this.props.value && this.props.value.val? 
						this.props.value.val ==='true'? 'false':'':'true'
		this.setState({value});
		this.props.filter(value);
	}
	
	filterDate (startDate, endDate){
		this.props.filter({startDate, endDate});
	}

	filter(type,e){
		let value  = e.target.value || '';
		switch(type){
			case 'text':
				value = value.replace('||','|').replace('**','*');
				if(value=='*'|| value=='|') value = '';
				if(this.props.value != value){
					this.props.filter(value);
				}
				break;
			case 'numeric':
				value = NumberInputValidation(value);
				if(this.props.value != value){
					if(!value || value[value.length-1].match(/[0-9]/)){
						this.props.filter(value);
					}
				}
				break;
		}
		
		
	}

}


ColumnHeader.defaultProps = {
	isEdit: false,
	isSorted:false,
	headerClass: '',
}

ColumnHeader.propTypes = {
	data: PropTypes.oneOfType([PropTypes.string,PropTypes.object]),
	isEdit: PropTypes.bool,
	filter: PropTypes.func.isRequired,
	type: PropTypes.oneOf(['numeric', 'text','date','bool']),
	isSorted: PropTypes.bool,
	sortByColumn: PropTypes.func.isRequired,
	reverseSort: PropTypes.func.isRequired,
	headerClass : PropTypes.string,
}