import React, {Component, PropTypes} from 'react'
import './s.css';

export default class MultySelect extends Component{
	constructor(props){
		super(props);
		this.state = {
			active:false,
			filter:'',
		}
		this.activate = this.activate.bind(this);
		this.renderContent = this.renderContent.bind(this);
		this.close = this.close.bind(this);
		this.renderPopUp = this.renderPopUp.bind(this);
		this.handleFilterChange = this.handleFilterChange.bind(this);
		this.renderValues = this.renderValues.bind(this);
		this.handleClick = this.handleClick.bind(this);
	}

	render(){
		const {className,width, height,color,background} = this.props;
		return (
			<div 
				className= {`bi_multiselect ${className}`} 
				style = {styles.cotainer(width, height,color,background)}>
				{this.renderContent()}
				{this.renderPopUp()}
			</div>
		)
	}

	renderContent(){
		const {value, defaultValue,height,color,background,width} = this.props;
		if(!this.state.active) return (
				<div onClick = {this.activate} style = {styles.perviwe(color,background)}>
						<div style = {styles.spansWrap(width,height)}>
							{value?this.renderValues(value):defaultValue}
						</div>
						<span 
							className="material-icons" 
							style ={styles.errow(color,height)}
						>
							keyboard_arrow_down
						</span>
				</div>)
		return (
			<input 
				type='search'
				autoFocus
				style = {styles.search(height,background,color)}
				onChange = {this.handleFilterChange}
				onKeyDown = {this.handleKey.bind(this)}
			/>
		)
	}



	renderValues(value){
		return value.split(',').map(val=>(
			<MultySelectValueElement
				key = {val}
				val= {val}
				dell = {this.dellItem.bind(this,val)}
				color = {this.props.color}
			/>
		))
	}

	handleFilterChange(e){
		this.setState({filter:e.target.value});
	}

	renderPopUp(){
		if(!this.state.active) return null;
		const {select,width,value} = this.props;
		const values = value.split(',');
		return(
			<div style = {styles.popup(width)}>
				{
					select.filter(item=>!isDataInArr(item,values))
						.filter(item=>isMatchFilter(item,this.state.filter))
						.map(ut=>(
							<MultySelectElement
								key = {ut}
								title = {ut}
								select = {this.addItem.bind(this,ut)}

							/>
						))
				}

			</div>
		)
	}

	addItem(item){
		if(!this.props.value){
			this.props.onChange(item);
			this.close()
			return;
		}
		
		const values = this.props.value.split(',');
		this.props.onChange([...values, item].join(','));
		this.close()
		
	}

	dellItem(item,e){
		e.stopPropagation();
		const filt = this.props.value.split(',').filter(li=>li!=item);
		if(filt.length ==0){
			this.props.onChange('');
			this.close()
			return;
		}
		this.props.onChange(filt.join(','));
		this.close()
	}

	activate(){
		this.setState({active:true,filter:''});
		document.addEventListener('click',this.handleClick)
	}
	close(){
		this.setState({active:false,filter:''});
		document.removeEventListener('click',this.handleClick)
	}

	handleClick(e){
		if(!e.target.closest(".bi_multiselect")){
				this.close();
				e.preventDefault();
				e.stopPropagation();
		}
	}

	handleKey(e){
		if(e.which == 27){
			this.close();
		}
	}

}



MultySelect.defaultProps = {
	width:100,
	height:40,
	className:'',
	color:'#394246',
	background:'transparent',
	defaultValue:'',
	value:''
}

MultySelect.propTypes = {
	width:PropTypes.number,
	height: PropTypes.number,
	className: PropTypes.string,
	color:PropTypes.string,
	background:PropTypes.string,
	defaultValue:PropTypes.string,
	value:PropTypes.string,
	select: PropTypes.arrayOf(PropTypes.string).isRequired,
	onChange: PropTypes.func.isRequired,
}

const styles = {
	cotainer:(width, height,color,background)=>({
		width:width - 6,
		height: `${height - 3}px`,
		borderBottom:`1px solid ${color}`,
		color,
		background,
		paddingLeft:'3px',
		paddingRight:'3px',
		paddingBottom:'2px',
		fontSize: '1em',
		lineHeight:`${height - 3}px`,
		cursor:'pointer',
		textAlign:'center',
		position:'relative'

	}),
	search:(height,background,color)=>({
		width:"100%",
		border:'0',
		height:`${height - 3}px`,
		padding:'0',
		textAlign: 'center',
		background,
		color
	}),
	popup:(width)=>({
		minWidth: width,
		maxHeight:'150px',
		overflowY:'auto',
		marginRight:'10px',
		zIndex:66,
	}),
	li:{
		background:"#394246",
		height:28,
		opacity:0.8,
		color:'white',
		cursor:'pointer',
		lineHeight: "28px"
	},
	perviwe:(color,background)=>({
		width:"100%",
		height: "100%",
		color,
		background,
		display:'flex',
		flexDirection:'row',
		flexWrap:'wrap',
		justifyContent:'space-between',
		alignItems:'center',
		overflow:'hidden',
	}),
	spanwrap:(color)=>({
		width:'1.75em',
		position:'relative',
		color,
	}),
	delicon:(color)=>({
		color:'white',
		position:'absolute',
		top:0,
		left:0,
		background:"lightcoral",
		backfaceVisibility:'hidden',
		WebkitBackfaceVisibility:'hidden',
		borderRadius:'50%',
		opacity:0
	}),
	errow:(color,height)=>({
		color,
		width:20,
		height,
		lineHeight:`${height}px`,
		textAlign:'center',
	}),
	spansWrap:(width, height)=>({
		width:`${width-27}px`,
		height:'100%',
		display:'flex',
		flexDirection:'row',
		flexWrap:'wrap',
		justifyContent:'center',
		alignItems:'center',
	})
}


const isMatchFilter=(value, filter)=>value.toUpperCase().includes(filter.toUpperCase());

const isDataInArr=(value, arr =[])=>{
	if(arr.length==0) return false;
	return arr.filter(item=>(item==value)).length>0;
};
const MultySelectElement = props =>(
	<div className = 'opacity-hover' style ={styles.li} onClick = {props.select}>
		{props.title}
	</div>
);


const MultySelectValueElement = props=>(
	<span style  = {styles.spanwrap(props.color)}>
		{props.val}
		<span 
			className = 'material-icons del-hover-opacity' 
			onClick = {props.dell}
			style  = {styles.delicon(props.color)}
		>
			clear
		</span>
	</span>
)








