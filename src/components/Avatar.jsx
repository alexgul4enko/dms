import React, {Component, PropTypes} from 'react';
import Avatar from 'react-toolbox/lib/avatar';
import './avatar.css';

export default class BI_Avatar extends Component{
	render (){
		const {FirstName,LastName, photo , letterStyle, avatarStyle, class_} =  this.props;
		const type = (photo && photo!='-')? 'avatar' : 'letter';
		switch(type){
			case 'letter':
				const letters = FirstName[0].toUpperCase() + LastName[0].toUpperCase();
				const style = Object.assign({},letterStyle_,letterStyle||{})
				return <div  
							style ={style}
							className = {class_}
						>
							{letters} 
						</div>
			default:
				return <img 
							width={(avatarStyle && avatarStyle.width)?avatarStyle.width  : 50}
							height = {(avatarStyle && avatarStyle.width)?avatarStyle.height  : 50}
							src = {`/api/MyUsers/avatar/${photo}`}
							style = {{borderRadius:"50%"}}
						/> 
		}
	}
}
BI_Avatar.defaultProps = {
	class_:''
}





const letterStyle_ = {
	width: 50,
	height:50,
	background:'#545967',
	lineHeight:"50px",
	borderRadius:'50%',
	fontSize:'1.3em'
}
