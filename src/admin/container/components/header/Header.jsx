import React, {Component, PropTypes} from 'react';
import './header.css';
import Tooltip from 'react-toolbox/lib/tooltip';
import Avatar from 'react-toolbox/lib/avatar';
import { browserHistory } from 'react-router';
import Dialog from 'react-toolbox/lib/dialog';
import ImageEditor from '../../../../components/imageEditor/ImageEditor';
import {Button} from 'react-toolbox/lib/button';

export default class  Header extends Component{
	constructor(props){
		super(props);
		this.state={
			active:false
		}
		this.openDialog = this.openDialog.bind(this);
		this.closeDialog = this.closeDialog.bind(this);
		this.editAvatar =this.editAvatar.bind(this);

	}
	openDialog(){
		this.setState({active:true});
	}
	closeDialog(){
		this.setState({active:false});
	}

	
	render (){
		const {FirstName,LastName} =  this.props.user ||{};
		return (
			<div className = "header">
				<button 
					className = {`material-icons ${this.props.tongle ? 'hide': 'show'}`}
					onClick = {this.props.tongleMenu}
				>
					menu
				</button>
				<img onClick = {()=>{browserHistory.push('/AdminPane')}} src="/files/big-logo-ba.png"/>
				<div className = "userData">
					{this.renderAvatar(this.props.user,this.openDialog)}
					<span className = "FIO">{`${FirstName} ${LastName}`}</span>
					<TooltipAvatar 
						tooltip="Выйти"
						icon = 'chevron_left'
						className = "AvatarIcon"
						onClick = {this.logOut}
					/>

				</div>

				<Dialog
					active={this.state.active}
					className = "admin-change-avatar"
					onOverlayClick={this.closeDialog}
				>
					<div className = "editos">
						<ImageEditor
							ref = "editor"
							photo = {this.props.user.Photo == '-' ? null : this.props.user.Photo}
							showError = {this.props.showError}
						/>
						<Button 
							className = "finish_edit"
							icon='save' 
							label='Сохранить' 
							flat 
							onMouseUp = {this.editAvatar}
							primary 
						/>
					</div>
				</Dialog>
			</div>
		)
	}

	editAvatar(){
		const {avatar,photo} = this.refs.editor.getPhotos();
		const {UserLogin :login, ID:id} = this.props.user;
		if(photo && photo.length > 70){
			this.props.editAvatar({avatar,photo,login,id})
		}
		this.closeDialog();
	}

	logOut(){
		// console.log("asdasd");
		deleteAllCookies();
		window.location.href = "/";
	}

	renderAvatar(user ={},openDialog){
		const {FirstName,LastName, Photo} =  this.props.user ||{};
		if(!FirstName && !LastName && !Photo) return null;
		if(!Photo || Photo == '-'){
			return <TooltipLeter
					title = {FirstName[0].toUpperCase() + LastName[0].toUpperCase()}
					style={{ display: 'inline-block' }} 
    				tooltip="Сменить аватар"
    				onClick = {openDialog}
				/>
		}
		return <TooltipImage
				Photo = {Photo}
				style={{ display: 'inline-block' }} 
    			tooltip="Сменить аватар"
    			onClick = {openDialog}
				/>

	}
}

const TooltipAvatar = Tooltip(Avatar);
const TooltipLeter = Tooltip(props => {
	const {theme,title,floating, accent, ...rest} = props||{};
	return <div {...rest } className = "TextAvatar">{title} {props.children}</div>
})
const TooltipImage = Tooltip(props => {
	const {theme,Photo,floating, accent, ...rest} = props||{};
	return <div {...rest } className = "TextAvatar">
				<img src = {`/api/MyUsers/avatar/${Photo}`} width = "40px" height = "40px" 
					style= {{borderRadius:"50%"}}/> 

				{props.children}
			</div>
})

const deleteAllCookies =() =>{
	const c = document.cookie.split("; ");
	for (let i in c) 
		document.cookie =/^[^=]+/.exec(c[i])[0]+"=;expires=Thu, 01 Jan 1970 00:00:00 GMT";    
}








