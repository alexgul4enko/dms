import React , { PropTypes} from 'react';
import Tooltip from 'react-toolbox/lib/tooltip';
const UserActivity = Tooltip(props => {
	const {theme,title,floating, accent,disableUser, ...rest} = props||{};
	return <span {...rest } 
				className = {`material-icons ${title}`}
				onClick = {props.disableUser}
			>
				{title} {props.children}
			</span>
})

UserActivity.propTypes = {
	title:PropTypes.string.isRequired,
	tooltip: PropTypes.string.isRequired,

}



export default UserActivity;