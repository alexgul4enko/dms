import React , { Component, PropTypes} from 'react';

export default class PasswordField extends Component {
	constructor(props){
		super(props);
		this.state = {
			active:false,
		}
		this.show = this.show.bind(this);
		this.hide= this.hide.bind(this);
	}

	render(){
		return this.state.active?(
			<span>
				<span>{this.props.pass} </span>
				<button className = "material-icons" onClick = {this.hide}>visibility_off</button>
			</span>
			):(
			<span>
				<span>&#9679; &#9679; &#9679; &#9679;</span>
				<button className = "material-icons" onClick = {this.show}>visibility</button>
			</span>
			)
	}

	show(){
		this.setState({active:true});
	}
	hide(){
		this.setState({active:false});
	}
}

