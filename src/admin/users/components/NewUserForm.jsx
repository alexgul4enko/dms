
import React, { PropTypes, Component } from 'react';
import Input from 'react-toolbox/lib/input';
import {Button} from 'react-toolbox/lib/button';
import ImageEditor from '../../../components/imageEditor/ImageEditor';

export default class NewUserForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			active:false,
			err: {}
		}
		this.openNewUserForm = this.openNewUserForm.bind(this);
		this.closeForm = this.closeForm.bind(this);
		this.renderForm = this.renderForm.bind(this);
	}
	render (){
		return this.props.active ? this.renderForm() : null
	}
	renderForm(){
		return (
			<div className  = "UserForm">
				<span className = 'material-icons cancel' onClick = {this.doNotSaveChanges.bind(this)}>
					cancel
				</span>
				<Input
					type='text'
					icon = 'person'
					label ="Имя"
					className = "NewUserForm-input"
					value = {this.state.FirstName}
					onChange={this.handleChange.bind(this, 'FirstName')}
					onBlur = {this.handleInputBlur.bind(this, 'FirstName')}
					onFocus = {this.handleInputFocus.bind(this, 'FirstName')}
					onKeyPress ={this.handleInputKey.bind(this, 'FirstName')}
					maxLength={50}
					error = {this.state.err.FirstName}
					autoFocus
					required
				/>
				<Input
					type='text'
					icon = 'person'
					label ="Фамилия"
					className = "NewUserForm-input"
					value = {this.state.LastName}
					onChange={this.handleChange.bind(this, 'LastName')}
					onBlur = {this.handleInputBlur.bind(this, 'LastName')}
					onFocus = {this.handleInputFocus.bind(this, 'LastName')}
					onKeyPress ={this.handleInputKey.bind(this, 'LastName')}
					error = {this.state.err.LastName}
					maxLength={50}
					ref = 'LastName'
					required

				/>
				<Input
					type='email'
					label ="Email"
					icon='email'
					className = "NewUserForm-input"
					value = {this.state.mail}
					onChange={this.handleChange.bind(this, 'mail')}
					onBlur = {this.handleInputBlur.bind(this, 'mail')}
					onFocus = {this.handleInputFocus.bind(this, 'mail')}
					onKeyPress ={this.handleInputKey.bind(this, 'mail')}
					error = {this.state.err.mail}
					maxLength={50}
					ref = 'mail'
					required
				/>
				<Input
					type='text'
					label ="Логин"
					icon='person_outline'
					className = "NewUserForm-input"
					value = {this.state.login}
					onChange={this.handleChange.bind(this, 'login')}
					onBlur = {this.handleInputBlur.bind(this, 'login')}
					onFocus = {this.handleInputFocus.bind(this, 'login')}
					onKeyPress ={this.handleInputKey.bind(this, 'login')}
					error = {this.state.err.login}
					maxLength={30}
					ref = 'login'
					required
				/>
				<Input
					type='password'
					label ="Пароль"
					icon='visibility'
					className = "NewUserForm-input"
					value = {this.state.pass}
					onChange={this.handleChange.bind(this, 'pass')}
					onBlur = {this.handleInputBlur.bind(this, 'pass')}
					onFocus = {this.handleInputFocus.bind(this, 'pass')}
					error = {this.state.err.pass}
					maxLength={10}
					ref = 'pass'
					required
				/>

				<ImageEditor
					photo = {this.state.photo}
					ref = "avatar"
					showError = {this.props.showError}
				/>


				<Button 
						className = "finish_edit"
						icon='save' 
						label='Сохранить' 
						flat 
						onMouseUp = {this.closeForm}
						primary 
					/>
			</div>	
		)
	}
	
	openNewUserForm(user={}){
		const user_ = Object.assign({},emptyUSer, user);
		this.setState({active:true,...user_,err:{}});
		this.props.tongleDialog();
	}
	closeForm(e){
		if(AreThereErrors(this.state.err) || IsThereEmptyData(this.state)){
			if(e.target && e.target.parentNode && e.target.parentNode.scrollTop){
				e.target.parentNode.scrollTop =0;
			}
			this.props.showError('Ошибка заполнения формы');
			return;
		}
		const {FirstName, LastName,act,id,login,mail,pass} = this.state;
		const {avatar,photo} = this.refs.avatar.getPhotos();
		if(!id){
			this.props.addUser({FirstName, LastName,act,id,login,mail,pass,avatar,photo})
		}
		else {
			this.props.updateUser({FirstName, LastName,act,id,login,mail,pass,avatar,photo})
		}
	}
	doNotSaveChanges(){
		this.props.tongleDialog();
	}
	handleChange(name, value){
		this.setState({[name]:value});
	}
	handleInputBlur(name,e){
		const {value} = e.target;
		if(!value){
			this.setState({err: {...this.state.err,[name]:'Поле не должно быть пустое' }});
		}
	}
	handleInputFocus(name){
		const {[name]: r, ...err} = this.state.err;
		this.setState({err});
	}
	handleInputKey(name, e){
		switch(e.which){
			case 13:
				switch(name){
					case 'FirstName':
						this.refs.LastName.getWrappedInstance().focus();
						return;
					case 'LastName':
						this.refs.mail.getWrappedInstance().focus();
						return;
					case 'mail':
						this.refs.login.getWrappedInstance().focus();
						return;
					case 'login':
						this.refs.pass.getWrappedInstance().focus();
						return;
				}
				return;
			default:
				e.stopPropagation();
				return;
		}
	}
}

const AreThereErrors = (err = {})=>{
	return Object.keys(err).length >0;
}


const IsThereEmptyData =(user = {}) =>{
	const {FirstName, LastName,login,mail,pass} = user;
	return !FirstName || !LastName || !login || !mail || !pass;
}

const emptyUSer = {
	FirstName:'',
	LastName:'',
	act:0,
	id:null,
	login:'',
	mail:'',
	pass:'',
	photo:'',
}