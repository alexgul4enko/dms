import React , { PropTypes} from 'react';
import UserActivity from './UserActivity';
const UserButtons= props =>{
	const status = props.active ? 'mood' : 'mood_bad';
	const tooltip= props.active ? 'Деактивировать' : 'Активировать';
	return(
		<div className = 'user-buttons'>
			<span className = 'material-icons edit' onClick = {props.editUser}>mode_edit</span>
			<UserActivity
				title = {status}
				tooltip = {tooltip}
				disableUser = {props.disableUser}
			/>
		</div>
	)
}

UserButtons.propTypes = {
	active: PropTypes.bool.isRequired,
	editUser : PropTypes.func.isRequired,
}

export default UserButtons;
