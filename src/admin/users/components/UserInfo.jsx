import React , { PropTypes} from 'react';
import BI_Avatar from '../../../components/Avatar';
import PasswordField from './PasswordField';
const UserInfo = props =>(
	<div className = "user-info">
		<div className = 'maindata'>
			<BI_Avatar
				FirstName = {props.FirstName}
				LastName = {props.LastName}
				photo = {props.photo}
			/>
			<span className ="FIO">{`${props.FirstName} ${props.LastName}`}</span>
		</div>
		<div className = 'mail'>
			<span className = 'material-icons'>mail</span>
			<span>{props.mail}</span>
		</div>
		<div className = 'logins'>
			<span>{`Логин: ${props.login}`}</span>
			<div>
				<span>{`Пароль: `}</span>
				<PasswordField pass = {props.pass}/>
			</div>
		</div>
	</div>
)


UserInfo.propTypes = {
	FirstName: PropTypes.string,
	LastName: PropTypes.string,
	photo: PropTypes.string,
	mail: PropTypes.string,
	login: PropTypes.string,
	pass: PropTypes.string,
}

export default UserInfo;