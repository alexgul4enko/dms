import React , {Component, PropTypes} from 'react';
import ListContainer, {List, ListHeader} from '../../../components/list/ListContainer';
import UserItem from './UserItem';

export default class UsersList extends Component {
	constructor(props){
		super(props);
		this.renderList = this.renderList.bind(this);
		this.renderItem = this.renderItem.bind(this);
	}
	render (){
		return arrayEmpty(this.props.users) ?  <Empty/> : this.renderList()
	}
	renderList(){
		return( 
			<ListContainer>
				<List
					filterPlaceHolder = "Поиск"
					renderItem = {this.renderItem}
					items = {this.props.users}
					filter = {this.filterContent}
					batch = {8}
				/>

			</ListContainer>
		)
	}
	
	renderItem (user,arrKey){
		return (
			<UserItem 
				key = {user.id} 
				user = {user}
				editUser = {this.props.openDialog}
				disableUser= {this.props.disableUser}
			/>
		)
	}

	filterContent(item,filt){
		return JSON.stringify(item).toLowerCase().includes(filt.toLowerCase());
	}
	
}
const arrayEmpty =(arr=[])=>(arr.length ==0)

const Empty = ()=>(<span className = "empty">Нет даных</span>)
