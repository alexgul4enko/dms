import React , {Component, PropTypes} from 'react';
import UserInfo from './UserInfo';
import UserButtons from './UserButtons';

export default class UserItem extends Component{
	constructor(props){
		super(props);
		this.editUser = this.editUser.bind(this);
		this.disableUser = this.disableUser.bind(this);
	}

	render(){
		return (
			<div className = "userItem">
				<UserInfo {...this.props.user}/>
				<UserButtons 
					editUser = {this.editUser}
					active ={this.props.user.act ==1}
					disableUser = {this.disableUser}
				/>
			</div>
		)
	}
	disableUser(){
		this.props.disableUser(this.props.user);
	}
	editUser(){
		this.props.editUser(this.props.user);
	}
}


UserItem.propTypes = {
	user : PropTypes.shape({
	    FirstName:PropTypes.string,
		LastName:PropTypes.string,
		act:PropTypes.number,
		id:PropTypes.number,
		login:PropTypes.string,
		mail:PropTypes.string,
		pass:PropTypes.string,
		photo:PropTypes.string,
  	}).isRequired,
  	editUser:PropTypes.func.isRequired,
  	disableUser: PropTypes.func.isRequired,
}













