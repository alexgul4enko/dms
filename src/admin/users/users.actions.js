import Constances from './Constances';
import httpPromice from '../../HttpPromice';
import {showError,tongleLoading} from '../app.actions';
const actions = {
	initUsers : ()=>{
		return (dispatch,getState)=>{
			dispatch(tongleLoading());
			httpPromice(`/api/MyUsers`,'GET')
				.then(data=>{
					dispatch(tongleLoading());
					dispatch(initUser(data));
					
				})
				.catch(err=>{
					dispatch(tongleLoading());
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	addUser:user=>{
		return (dispatch,getState)=>{
			const {FirstName,LastName,avatar,login,mail,pass,photo} =user;
			httpPromice('/api/MyUsers','POST',{FirstName,LastName,login,mail,pass} )
				.then(newUser=>{
					if(!photo || photo.length <70 ) {
						return new Promise((resolve,reject)=>{resolve(newUser)});
					}
					return httpPromice(`/api/MyUsers/${newUser.id}/avatar`,'PUT',{login,avatar,photo})
				})
				.then(withAvatar=>{
					dispatch(addUser(withAvatar));
					dispatch(tongleDialog());
				})
				.catch(err=>{
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	updateUser:user=>{
		return (dispatch,getState)=>{
			const {FirstName,LastName,avatar,login,mail,pass,photo,id}=user;
			httpPromice(`/api/MyUsers/${id}`,'PUT',{FirstName,LastName,login,mail,pass} )
				.then(newUser=>{
					if(!photo || photo.length <70 )  {
						return new Promise((resolve,reject)=>{resolve(newUser)});
					}
					return httpPromice(`/api/MyUsers/${newUser.id}/avatar`,'PUT',{login,avatar,photo})
				})
				.then(withAvatar=>{
					dispatch(updateUser(withAvatar));
					dispatch(tongleDialog());
				})
				.catch(err=>{
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	disableUser:user=>{
		return (dispatch,getState)=>{
			const {act,id}=user;
			const activity = act ==1 ? 0 :1;
			httpPromice(`/api/MyUsers/${id}`,'DELETE',{act:activity})
				.then(newUser=>{
					dispatch(updateUser(newUser));

				})
				.catch(err=>{
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	tongleDialog:()=>({
		type:Constances.MYUSERS_TONGLE_DIALOG
	}),
	showError
}

const tongleDialog = ()=>({
		type:Constances.MYUSERS_TONGLE_DIALOG
});
const tongleUser = rehidrate =>({
	type:Constances.MYUSERS_DISABLEUSER,
	rehidrate,
});

const addUser  = rehidrate =>({
	type:Constances.MYUSERS_ADD_USER,
	rehidrate,
});

const updateUser  = rehidrate =>({
	type:Constances.MYUSERS_UPDATE_USER,
	rehidrate,
});

const initUser = rehidrate =>({
	type:Constances.INIT_MYUSERS_LIST,
	rehidrate,
});


export default  actions;

export {initUser};

