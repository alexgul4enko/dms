import Constances from './Constances';
import {combineReducers}  from 'redux';

const rootUsersReducer = combineReducers({
  users,
  show
});

function show  (state=false,action){
	switch(action.type){
		case Constances.MYUSERS_TONGLE_DIALOG:
			return !state;
		default:
			return state;
	}
};

function users  (state=[],action){
	switch(action.type){
		case Constances.INIT_MYUSERS_LIST:
			return action.rehidrate;
		case Constances.MYUSERS_UPDATE_USER:
			return state.map(user=>{
				if(user.id == action.rehidrate.id) return action.rehidrate;
				return user;
			});
		case Constances.MYUSERS_DISABLEUSER:
			return state.map(user=>{
				if(user.id == action.rehidrate.id) return action.rehidrate;
				return user;
			});
		case Constances.MYUSERS_ADD_USER:
			return [...state, action.rehidrate];
		default:
			return state;
	}
};

export default rootUsersReducer;


