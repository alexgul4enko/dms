import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './users.actions';
import './users.css';
import UsersList from './components/UsersList'
import {Button} from 'react-toolbox/lib/button';
import NewUserForm from './components/NewUserForm';

class Users extends Component{
	constructor(props){
		super(props);
		this.addUser = this.addUser.bind(this);
		this.editUser = this.editUser.bind(this);
	}

	render (){
		return( <div id = "ad-MyUsers" className = "content_div">
					<Button 
						className = "add_invoice"
						icon='person_add' 
						label='Создать пользователя' 
						flat 
						onMouseUp = {this.addUser}
						primary 
					/>

					<div className = "add_User_content">
						<UsersList
							users ={this.props.users}
							openDialog = {this.editUser}
							disableUser = {this.props.disableUser}
						/>
						<NewUserForm 
							ref = 'newUser'
							addUser = {this.props.addUser}
							showError = {this.props.showError}
							updateUser = {this.props.updateUser}
							active ={this.props.show}
							tongleDialog = {this.props.tongleDialog}
						/>
					</div>
				</div>)
	}

	editUser(user){
		this.refs.newUser.openNewUserForm(user);
	}

	addUser(){
		this.refs.newUser.openNewUserForm({});
	}
	componentWillMount() {
		if(isArrEmpty(this.props.users)) this.props.initUsers();
	}
}

const isArrEmpty =(arr=[])=>(arr.length == 0);

const mapStateToProps = (state= {}) =>({
  	users : state.myUsers.users,
  	show : state.myUsers.show,
    user : state.user,
})

const  mapDispatchToProps = dispatch => bindActionCreators(actions,dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(Users);