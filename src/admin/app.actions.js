import Constances from './Constances';
import httpPromice from '../HttpPromice';
import { browserHistory } from 'react-router';

const appActions = {
	getUser : ()=>{
		return (dispatch,getState)=>{
			let UserData;
			httpPromice('/api/login','GET')
				.then(data=>{
					dispatch(setupUser(data))
				})
				.catch(err=>{
					console.log(err)
				})
		}
	},

	closeError: ()=>{
		return showError('');
	},
	editAvatar:data=>{
		return (dispatch,getState)=>{
			
			const {id,avatar,login,photo} =data;
			httpPromice(`/api/MyUsers/${id}/avatar`,'PUT',{login,avatar,photo})
			.then(user=>{
				dispatch(updatePhoto(user.photo));
			})
			.catch(err=>{
				dispatch(showError(err.message ? err.message:err));
			})
		}
	},
	showError:(err)=>showError(err)
}
const updatePhoto = rehidrate =>({
	type:Constances.EDIT_MY_AVATAR,
	rehidrate,
});

const showError = rehidrate =>({
	type:Constances.SHOW_ERROR,
	rehidrate,
});
const tongleLoading =()=>({
	type:Constances.TONGLE_LOADING,
});


const setupUser = rehidrate =>({
	type:Constances.SETUP_USER,
	rehidrate,
});

export default appActions;
export {showError,tongleLoading}