import Constances from './Constances';

export default function UsersTT  (state=[],action){
	switch(action.type){
		case Constances.TTUSERS_INIT_TT_USERS:
			return action.rehidrate;
		case Constances.TTUSERS_DELETE_TTUSER:
			return state.filter(item=>(item.id!=action.rehidrate));
		case Constances.TTUSERS_ADD_TTUSER:
			return [...state, action.rehidrate];
		default:
			return state;
	}
};


function TT (state=[],action){
	switch(action.type){
		case Constances.TTUSERS_INIT_TT:
			return action.rehidrate;
		default:
			return state;
	}
}

export {TT}