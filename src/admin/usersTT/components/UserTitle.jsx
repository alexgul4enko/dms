import React, { PropTypes,Component } from 'react';
import BI_Avatar from '../../../components/Avatar';
import { DragSource  } from 'react-dnd';
import {compose} from 'redux';
import { findDOMNode } from 'react-dom';


const dndSource = {
  beginDrag(props) {
  	const {id,login} = props;
    return {
      data: {id,login},
    };
  },
  endDrag (props, monitor, component){
    if(props.onDrop){
      const source = monitor.getItem().data;
      const target = monitor.getDropResult();
      props.onDrop( target,source)
    }
  }
};


@DragSource('usersTT_USER', dndSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
}))
class UserTitle extends Component{

	render(){
		const {login, connectDragSource, FirstName,LastName,photo,act } = this.props;
		return login ? connectDragSource(
			<div className = "userAvatar">
				<BI_Avatar
					FirstName = {FirstName}
					LastName = {LastName}
					photo = {photo}
					letterStyle ={{width:40, height:40,lineHeight:"40px"}}
					avatarStyle ={{width:40, height:40}}
				/>
				<span>{login}</span>
				<span 
					className = "material-icons"
					style={{color:act=='1'?'#7ab825':'#ef6439'}}
				>
					{act=='1'?'mood':'mood_bad'}
				</span>
			</div> ):null
	}
	
}
UserTitle.defaultProps = {
	FirstName:'',
	LastName:'',
	photo:'-',

}
UserTitle.propTypes = {
	FirstName: PropTypes.string,
	LastName: PropTypes.string,
	photo: PropTypes.string,
	login: PropTypes.string,
	act:PropTypes.number
}

export default UserTitle;