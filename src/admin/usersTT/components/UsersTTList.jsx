import React, { PropTypes, Component } from 'react';
import ListContainer, {List,textfilter} from '../../../components/list/ListContainer';
import { DropTarget } from 'react-dnd';
import UsersTTListElement from './UsersTTListElement'


const boxTarget = {
  drop(props) {
    return props.user;
  },
  canDrop(props){
    return props.isDrobable;
  }
};

@DropTarget('usersTT_TT', boxTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget(),
}))
class UsersTTList extends Component{
	constructor(props){
		super(props);
		this.renderList = this.renderList.bind(this);
		this.renderItem = this.renderItem.bind(this);
	}
	render(){
		if(this.props.user == null ) return null;
		const { connectDropTarget } = this.props;
		return connectDropTarget(
			<div className = "userDropTarget">
				{this.renderList()}
			</div>
		)
	}

	renderList(){
		const {login} = this.props.user;
		const list = this.props.usersTT.filter(ut=>(ut.usr == login)).map(item=>{
			return {...item,ttdata:this.ttobj[item.tt]};
		})
		if(isArrEmpty(list)) return <h1>Список пуст перетащите сюда ТТ или перетажите аватар на ТТ из списка справа</h1>;
		return <ListContainer class = "user_TT">
					<List
						filterPlaceHolder = "Поиск"
						renderItem = {this.renderItem}
						items = {list}
						filter = {this.filterContent}
						batch = {80}

					/>
				</ListContainer>
	}

	renderItem (userTT,arrKey){
		return <UsersTTListElement
					key = {userTT.id+'_UT'}
					item = {userTT}
					delUserTT = {this.props.delUserTT}
				/>
	}
	filterContent(item,filt){
		return filtList(item.ttdata, filt);
	}
	componentWillMount() {
		this.ttobj = this.props.tt.reduce((prev,next)=>{
			return {...prev, [next.id]:next};
		},{})
	}

	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.usersTT.length !=this.props.usersTT.length ||
			(nextProps.user && nextProps.user.id) != (this.props.user && this.props.user.id)
	}
}

const filtList = (data={}, filter)=>{
	const {name, addr} = data;
	return textfilter(filter, name) || textfilter(filter, addr) ;
}

const isArrEmpty =(arr=[])=>arr.length==0;

UsersTTList.propTypes = {
	user: PropTypes.object,
	list: PropTypes.array,
	isDrobable: PropTypes.bool.isRequired,
}

export default UsersTTList;