import React, { PropTypes, Component } from 'react';
import Select from '../../../components/select/Select'
import UserTitle from './UserTitle';
import UsersTTList from './UsersTTList';

export default class UsersPart extends Component {
	constructor(props){
		super(props);
		this.state={
			user:null,
		}
		this.selectUser = this.selectUser.bind(this);
		this.userDrop = this.userDrop.bind(this);
	}

	render (){
		const selecteedUserName = this.state.user ? `${this.state.user.FirstName} ${this.state.user.LastName}`:'';
		return (
			<div className = "parts">
				<Select
					data = {this.props.users}
					onChange = {this.selectUser}
					value = {selecteedUserName}
					icon = "people"
					parseData = {this.parseUser}
					placeHolder = "Выберите пользователя"
				/>
				<UserTitle 
					{...this.state.user || {}} 
					onDrop = {this.userDrop}
				/>
				{
					this.props.tt.length>0 ? 
							<UsersTTList
								list = {this.state.list}
								isDrobable = {true}
								user={this.state.user}
								tt = {this.props.tt}
								delUserTT = {this.props.delUserTT}
								usersTT = {this.props.usersTT}
							/>:null

				}
				
			</div>
		)
	}

	userDrop(target, source){
		if(!target){
			this.props.showError('Пользователя нужно перетаскивать на ТТ');
			return ;
		}
		const {login} = source;
		const {id,users} = target;
		if(Array.isArray(users) && 
				users.filter(item=>(item.usr == login)).length >0){
			this.props.showError('ТТ уже закреплена за пользователем');
			return;
		}
		this.props.addUserTT({usr: login, tt: id});
	}
	shouldComponentUpdate(nextProps, nextState) {
		return (nextState.user && nextState.user.id) != (this.state.user && this.state.user.id) ||
			nextProps.users.length != this.props.users.length||
			nextProps.usersTT.length !=this.props.usersTT.length 

	}

	parseUser(item, i){
		const {LastName, FirstName,id, login} = item;
		return {
			key :login,
			title: `${FirstName} ${LastName}`
		}
	}
	selectUser(user){
		const {login} = user;
		this.setState({user});
	}
}

UsersPart.propTypes = {
	users: PropTypes.array.isRequired,
	showError: PropTypes.func.isRequired,
}