import React, { PropTypes, Component } from 'react';
import ListContainer, {List,textfilter,ListHeader} from '../../../components/list/ListContainer';
import TTWithUsers from './TTWithUsers'
export default class TTPart extends Component {
	constructor(props){
		super(props);
		this.renderItem  = this.renderItem.bind(this);
		this.ttDrop = this.ttDrop.bind(this);
		this.rendercontent = this.rendercontent.bind(this)

	}
	render (){
		return (
			<div className = "parts">
				{this.rendercontent()}
			</div>
		)
	}

	rendercontent(){
		if(this.props.tt.length == 0) return <h1 className="no-data-err">Нет доступных Вам ТТ обратитесь к администратору</h1>
		return (
			<ListContainer >
				<ListHeader
					class ="TT_header"
					title ="Торговые точки"
				/>
				<List
					filterPlaceHolder = "Поиск"
					renderItem = {this.renderItem}
					items = {this.props.tt}
					filter = {this.filterContent}
					class= "TTListUsers"
					batch = {20}
				/>
			</ListContainer>
		)

	}

	ttDrop(target, source){
		if(!target){
			this.props.showError('TT нужно перетаскивать пользователя')
			return;
		}
		const{id :tt_id,users} = source;
		const {id :usr_id,login} = target;
		if(Array.isArray(users) && 
				users.filter(item=>(item.usr == login)).length >0){
			this.props.showError('ТТ уже закреплена за пользователем');
			return;
		}
		this.props.addUserTT({usr: login, tt: tt_id});
	}

	renderItem (userTT,arrKey){
		const getUserdata = (login)=>{
			return this.props.users.filter(usr=>(usr.login == login))[0];
		}
		return <TTWithUsers
					key = {userTT.id+'_TT'}
					item = {userTT}
					getUserdata = {getUserdata}
					delUserTT = {this.props.delUserTT}
					onDrop = {this.ttDrop}

				/>
	}
	filterContent(item,filt){
		return filtList(item, filt);
	}
}


const filtList = (data={}, filter)=>{
	const {name, addr} = data;
	return textfilter(filter, name) || textfilter(filter, addr) ;
}