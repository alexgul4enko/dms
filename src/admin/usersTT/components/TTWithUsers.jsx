import React, { PropTypes, Component } from 'react';
import BI_Avatar from '../../../components/Avatar';
import Tooltip from 'react-toolbox/lib/tooltip';
import { DragSource, DropTarget  } from 'react-dnd';


const dndSource = {
  canDrag(props){
    return true;
  },
  beginDrag(props) {
    return {
      data: props.item,
    };
  },
  endDrag (props, monitor, component){
      const source = monitor.getItem().data;
      const target = monitor.getDropResult();
      props.onDrop(target,source)
  }
};

const dndTarget = {
  drop(props){
    return props.item;
  }
}




@DropTarget('usersTT_USER', dndTarget, (connect, monitor) => ({
  connectDropTarget: connect.dropTarget()
}))
@DragSource('usersTT_TT', dndSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  connectDragPreview: connect.dragPreview(),
}))
class TTWithUsers extends Component {
	constructor(props){
		super(props);
		this.renderAvatars = this.renderAvatars.bind(this);
	}
	render (){
		const {name, addr} = this.props.item;
		const {connectDropTarget,connectDragSource ,connectDragPreview} = this.props;
		return connectDropTarget(connectDragPreview(
				<div className = "TT-li">
					<div className = 'main-data'>
						{
							connectDragSource(<span className= "material-icons">drag_handle</span>)
						}
						<div  className = 'tt-info'>
							<span>{name}</span>
							<span>{addr}</span>
							
						</div>
					</div>
					<div className = 'tt-users'>
						{this.renderAvatars()}
					</div>
				</div>
			))
	}
	renderAvatars(){
		if(!this.props.item || !Array.isArray(this.props.item.users)) return null;
		return this.props.item.users.map((usr,i)=>{
			const {FirstName,LastName,photo} = this.props.getUserdata(usr.usr);
			return <TTUSerAvatas
						key = {usr.usr+'intt'}
						FirstName = {FirstName}
						LastName = {LastName}
						photo = {photo}
						tooltip = {`${FirstName} ${LastName}`}
						onDeleteClick = {this.props.delUserTT.bind(null,usr.id)}
					/>
		})
	}

	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.item.id!=this.props.item.id ||
			(this.props.item.users && this.props.item.users.length) !=(nextProps.item.users && nextProps.item.users.length)
	}
}



const TTUSerAvatas = Tooltip(props=>{
	const {theme,photo,FirstName,LastName,floating, onDeleteClick,accent, ...rest} = props||{};
	return (
		<div {...rest } className = 'usrTT-avatar'>
			<BI_Avatar
				FirstName = {props.FirstName}
				LastName = {props.LastName}
				photo = {props.photo}
				letterStyle ={{width:40, height:40,lineHeight:"40px"}}
				avatarStyle ={{width:40, height:40}}
			/>
			<div 
				className = 'material-icons' 
				onClick = {props.onDeleteClick}
			>
				delete_forever
			</div>
			{props.children}
		</div>
	)
})


export default TTWithUsers;





