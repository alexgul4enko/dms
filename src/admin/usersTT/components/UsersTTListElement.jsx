import React, { PropTypes, Component } from 'react';


export default class UsersTTListElement extends Component{
	constructor(props){
		super(props)
	}
	render(){
		const {ttdata } = this.props.item;
		if(ttdata){
			const {name, addr} = ttdata;
			return (
				<div className = "usersTT-li">
					<span>{name}</span>
					<span>{addr}</span>
					<span 
						className = 'material-icons' 
						onClick = {this.props.delUserTT.bind(null,this.props.item.id)}
					>
						delete_forever
					</span>
				</div>
			)
		}
		return null;
		
	}


	shouldComponentUpdate(nextProps, nextState) {
		return nextProps.item.id!=this.props.item.id
	}
}

