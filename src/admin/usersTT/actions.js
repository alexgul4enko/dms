import Constances from './Constances';
import httpPromice from '../../HttpPromice';
import {showError,tongleLoading} from '../app.actions';
import {initUser} from '../users/users.actions';
import uuid from 'uuid';
const actions = {
	initData : ()=>{
		return (dispatch,getState)=>{
			let check = checker(3, ()=>dispatch(tongleLoading()))
			const {myUsers, TT, UsersTT} = getState();
			dispatch(tongleLoading());
			
			
			if(!check(myUsers.users)){
				httpPromice(`/api/MyUsers`,'GET')
					.then(data=>{
						dispatch(initUser(data));
						check(data,true);
					})
					.catch(err=>{
						check([],true);
						dispatch(showError(err.message ? err.message:err));
					})
			}
			if(!check(TT)){
				httpPromice(`/api/TT`,'GET')
					.then(data=>{
						dispatch(initTTList(data));
						check(data,true);
					})
					.catch(err=>{
						check([],true);
						dispatch(showError(err.message ? err.message:err));
					})
			}
			if(!check(UsersTT)){
				httpPromice(`/api/UsersTT`,'GET')
					.then(data=>{
						dispatch(initTTUsers(data));
						check(data,true);
					})
					.catch(err=>{
						check([],true);
						dispatch(showError(err.message ? err.message:err));
					})
			}
			

		}
	},
	addUserTT:data=>{
		return (dispatch,getState)=>{
			httpPromice(`/api/UsersTT`,'POST',data)
				.then(data_=>{
					dispatch(addTTUser(data_));
				})
				.catch(err=>{
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	delUserTT:data=>{
		return (dispatch,getState)=>{
			httpPromice(`/api/UsersTT/${data}`,'DELETE')
				.then(data_=>{
					dispatch(deleteTTUser(data));
				})
				.catch(err=>{
					dispatch(showError(err.message ? err.message:err));
				})
		}
	},
	showError
}


const addTTUser = rehidrate =>({
	type:Constances.TTUSERS_ADD_TTUSER,
	rehidrate,
});
const deleteTTUser = rehidrate =>({
	type:Constances.TTUSERS_DELETE_TTUSER,
	rehidrate,
});


const initTTUsers = rehidrate =>({
	type:Constances.TTUSERS_INIT_TT_USERS,
	rehidrate,
});


const initTTList = rehidrate =>({
	type:Constances.TTUSERS_INIT_TT,
	rehidrate,
});

function checker (co ,callback){
	let loadet = 0;
	return (arr=[],anyway)=>{
		if(arr.length > 0 || anyway){
			if(++loadet == co) callback();
			return true;
		}
		return false;
	}
}

export default  actions;
const {initData} = actions;
export {initTTList,initData }

