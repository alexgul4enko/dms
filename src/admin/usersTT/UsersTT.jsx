import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import './index.css';
import UsersPart from './components/UsersPart';
import TTPart from './components/TTPart';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import  PureRenderMixin from 'react-addons-pure-render-mixin';
@DragDropContext(HTML5Backend)
class UsersTT extends Component{

	constructor(props){
		super (props);

	}
	render (){
		return (
				<div id = "USERS_TT" className = "content_div">
					<UsersPart
						users ={this.props.users}
						usersTT = {this.props.usersTT}
						tt = {this.props.tt}
						showError = {this.props.showError}
						delUserTT = {this.props.delUserTT}
						addUserTT = {this.props.addUserTT}
					/>
					<TTPart
						tt = {this.props.ttwithusr}
						users ={this.props.users}
						delUserTT = {this.props.delUserTT}
						showError = {this.props.showError}
						addUserTT = {this.props.addUserTT}
					/>
				</div>
			)
	}

	

	shouldComponentUpdate(nextProps, nextState) {
		if(nextProps.loading) return false;
		return this.props.users.length>0 && this.props.tt.length >0;
	}


	componentDidMount() {
		const {users, usersTT,tt} = this.props;
		if(users.length > 0 && usersTT.length > 0 && tt.length >0){
			return;
		}
		this.props.initData();
	}
}


const  mapStateToProps =(state= {})=> {
	const tt_obj = listUSRRToObj(state.UsersTT);
	return {
		users : state.myUsers.users,
		tt:state.TT,
		usersTT :state.UsersTT,
		loading : state.loading.loading,
		ttwithusr : state.TT.map(item=>({...item, users:tt_obj[item.id]}))

	}
}


const listUSRRToObj = (arr=[])=>{
	return arr.reduce((prev,next)=>{
		const {tt,id,usr} = next;
		if(prev[tt]) return  {...prev, [tt]:[...prev[tt], {id,usr}] } ;
		return {...prev,[tt]:[{id,usr}] }
	},{})
}



const mapDispatchToProps =dispatch => bindActionCreators(actions,dispatch);


export default connect(mapStateToProps, mapDispatchToProps)(UsersTT);