import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import actions from './actions';
import RootFilter from './components/RootFilter';
import './index.css';
import TableRoutes from './components/TableRoutes';

class Routes extends Component{
	constructor(props){
		super(props);
		this.state = {
			user:null,
			from : null,
			to: null,
			showWeekend:false,
			tongle: false,
			val:''
		}
		this.userChanged = this.userChanged.bind(this);
		this.switchView = this.switchView.bind(this);
		this.tongleWeekends = this.tongleWeekends.bind(this);
		this.selectDays = this.selectDays.bind(this);
		this.renderViews = this.renderViews.bind(this);
		
	}


	render (){
		const userName = this.state.user == null ? '' :
						`${this.state.user.FirstName} ${this.state.user.LastName}`
		return (
			 <div id = "adm_routes" className = "content_div">
			 	<RootFilter
			 		users = {this.props.users}
			 		user = {userName}
			 		selectUser = {this.userChanged}
			 		switchView = {this.switchView}
			 		tongle = {this.state.tongle}
			 		tongleWeekends = {this.tongleWeekends}
			 		weekends = {this.state.showWeekend}
			 		selectDays = {this.selectDays}
			 	/>
			 	<div className= "routes-container" id= "calculdate-width">
			 		{this.renderViews()}
			 	</div>
			 </div>
		)
	}
	renderViews(){
		if(!this.state.user) return null;
		const {from :from_,to} = this.state;
		return <TableRoutes
					showWeekend = {this.state.showWeekend}
					usersTT = {this.props.usersTT}
					tt = {this.props.tt}
					routes = {this.props.routes}
					user = {this.state.user}
					dateRange = {{from_,to}}
				/>
	}
	userChanged(user){
		this.setState({user});
		this.props.getRoutesByUsers(user)
	}
	switchView(){
		this.setState({tongle:!this.state.tongle});
	}
	tongleWeekends (){
		this.setState({showWeekend:!this.state.showWeekend});
	}
	selectDays(from_,to){
		this.setState({from:from_,to});
	}
	componentWillMount() {
		const {users, usersTT,tt} = this.props;
		if(users.length > 0 && usersTT.length > 0 && tt.length >0){
			return;
		}
		this.props.initData();
	}
}

const ttToObj =(tt=[])=>tt.reduce((prev,next)=>{
	return {...prev, [next.id]:next}
},{})

const  mapStateToProps =(state= {})=> {
	return {
		users : state.myUsers.users,
		tt:ttToObj(state.TT),
		usersTT :state.UsersTT,
		routes:state.usr_routes,
	}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions,dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes);