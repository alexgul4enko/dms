import React, { PropTypes, Component } from 'react';
import TableComponent from '../../../components/table/TableComponent';
import moment from 'moment';


export default class TableRoutes extends Component {
	constructor(props){
		super(props);
		this.state={
			width: window.innerWidth,
			height: window.innerHeight,
			columns : getColums(),
			routes : routesToObj(props.routes)
		}
		this.handleResize = this.handleResize.bind(this);
		this.resizeColumn = this.resizeColumn.bind(this);
	}

	componentWillUpdate(nextProps, nextState) {
		if((nextProps.user  && nextProps.user.login) != (this.props.user  && this.props.user.login) 
			){
			nextState.routes = routesToObj(nextProps.routes)
		}
	}
	render(){
		const columns = this.state.columns.filter(col=>{
			if (col.group == "ТТ") return true;
			const dayWeek = col.title.format('dd');
			if(dayWeek == 'вс' || dayWeek == 'сб') 
				return this.props.showWeekend && constDateinRange(this.props.dateRange,col.title );
			return constDateinRange(this.props.dateRange,col.title );
		})
		const {login} = this.props.user;
		const data = this.props.usersTT
			.filter(ut=>{
				return ut.usr == login;
			})
			.map(ut=>{
				const TT = this.props.tt[ut.tt];
				return {outletID:TT.id, outletName:TT.name,outletPeriod:ut.per}
			})
		return <TableComponent
					width = {this.state.width}
					height = {this.state.height}
					columns = {columns}
					data = {data}
					resizeColumn = {this.resizeColumn}
					dataID = {login}
				/>
	}

	handleResize(){
		let calsoze = window.getComputedStyle(document.getElementById('calculdate-width'));
		this.setState({width: parseInt(calsoze.width),
						height: parseInt(calsoze.height)
				   });
	}
	componentDidMount() {
		window.addEventListener('resize', this.handleResize);
		this.handleResize();
	}
	componentWillUnmount() {
		window.removeEventListener('resize',this.handleResize);
	}
	resizeColumn(data){
		const {width,columnKey} = data;
		const columns  = this.state.columns.map(c=>{
			if(c.dbName == columnKey) return Object.assign({},c,{width});
			return c;
		})
		this.setState({columns});
	}
}

const constDateinRange =(range,date)=>{
	const {from_, to} = range;
	if(!from_ || !to ) return true;
	return  date.isSameOrAfter(from_,'day') &&  date.isSameOrBefore(to, 'day');
}

const calculateData = (user,usersTT,tt )=>{
	const {login} = user;
	return usersTT
			.filter(ut=>{
				return ut.usr == login;
			})
			.map(ut=>{
				const TT = this.props.tt[ut.tt];
				return {outletID:TT.id, outletName:TT.name,outletPeriod:ut.per}
			})
}

const routesToObj = (arr=[])=>{
	return arr.reduce((prev,next)=>{
		const {day,id,tt} = next;

		const {[tt]:a} = prev;
		if(!a) return {...prev, [tt]:{[moment(day).format('DD-MM-YY')]:id}};
		return {...prev, 
				[tt]: {...a, [moment(day).format('DD-MM-YY')]:id}
			};
	},{});
}
const getColums = ()=>{
	let columns = [	
					{
						dbName: "outletID",
						title: "ID",
						type: "numeric",
						readOnly: true,
						editor: "text",
						width:1,
						group: "ТТ",
						isFixed: true
					},
					{
						dbName: "outletName",
						title: "Название",
						type: "text",
						readOnly: true,
						editor: "text",
						width:230,
						group: "ТТ",
						isFixed: true
					},
					{
						dbName: "outletPeriod",
						title: "Периодичность",
						type: "text",
						readOnly: false,
						editor: "text",
						width:130,
						group: "ТТ",
						isFixed: true
					},
				];
	for (let i=0;i<32;i++){
		columns.push({
			dbName: `dd_${i}`,
			title: moment().add(i,'days'),
			type: "bool",
			readOnly: false,
			editor: "text",
			width:100,
			group: "Календарь",
			isFixed: false
		})
	}
	return columns;
}