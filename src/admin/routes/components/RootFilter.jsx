import React, { PropTypes, Component } from 'react';
import Select from '../../../components/select/Select'
import Tooltip from 'react-toolbox/lib/tooltip';
import DatesFilter from './DatesFilter';

export default class RootFilter extends Component{
	constructor(props){
		super(props)
	}
	render(){
		return (
			<div className = "routes-filters">
				<Select
					data = {this.props.users}
					onChange = {this.props.selectUser}
					value = {this.props.user}
					icon = "people"
					parseData = {this.parseUser}
					placeHolder = "Выберите пользователя"
					width = '280px'
					height = '30px'
				/>
				<div className = "filter-dates">
					<span className = 'material-icons'>date_range</span>
					<DatesFilter
						changeFilter = {this.props.selectDays}
					/>
				</div>
				<div className = "filter-butons">
					<Weekends
						tongleWeekends = {this.props.tongleWeekends}
						tongle = {this.props.weekends}

					/>
					<VIEW_BUTTON
						viewmode = {this.props.tongle}
	    				tooltip={this.props.tongle? "Переключится на карту" :"Переключится на таблицу"}
	    				onClick = {this.props.switchView}
					/>
				</div>
			</div>
		)
	}

	changeFilter(a,b){
		console.log({a,b})
	}

	parseUser(item, i){
		const {LastName, FirstName,id, login} = item;
		return {
			key :login,
			title: `${FirstName} ${LastName}`
		}
	}
}



RootFilter.propTypes = {
	users: PropTypes.array.isRequired,
	selectUser: PropTypes.func.isRequired,
	user :  PropTypes.string,
	switchView: PropTypes.func.isRequired,
	tongle : PropTypes.bool.isRequired,
	weekends: PropTypes.bool.isRequired,
	tongleWeekends: PropTypes.func.isRequired,
	selectDays: PropTypes.func.isRequired,

}


const Weekends = props =>(
	<button className = "filters-weekends" onClick = {props.tongleWeekends}>
		<span>Выходные: </span>
		<span className = "material-icons">{props.tongle? 'check_box':'check_box_outline_blank'}</span>
	</button>
)

const VIEW_BUTTON = Tooltip(props => {
	const {theme,viewmode,floating, accent, ...rest} = props||{};
	return <button {...rest } className = "Swith-view-button">
				<div className = 'material-icons'>visibility</div>
				<div>{viewmode?"Карта":"Таблица"}</div>

				{props.children}
			</button>
})










